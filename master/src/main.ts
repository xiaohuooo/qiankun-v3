import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { registerMicroApps, start } from "qiankun";
import microApps from "./micro-app";
createApp(App).use(store).use(router).mount("#app");
// 给子应用配置加上loader方法
const apps = microApps.map((item) => {
  return {
    ...item,
  };
});
console.log(apps, "-----------apps");
registerMicroApps(apps);
start({
  prefetch: "all", // 可选，是否开启预加载，默认为 true。
  sandbox: true, // 可选，是否开启沙箱，默认为 true。// 从而确保微应用的样式不会对全局造成影响。
  singular: true, // 可选，是否为单实例场景，单实例指的是同一时间只会渲染一个微应用。默认为 true。
});
