import store from "./store";
const microApps = [
  {
    name: "web-data-supervision",
    entry: "http://localhost:8098/web-data-supervision/",
    activeRule: "/web-data-supervision",
  },
  {
    name: "web-data-tpp",
    entry: "http://localhost:8100/web-data-tpp/",
    activeRule: "/web-data-tpp",
  },
  {
    name: "web-data-pdg",
    entry: "http://localhost:8101/web-data-pdg/",
    activeRule: "/web-data-pdg",
  },
  {
    name: "web-performance-eval",
    entry: "http://localhost:8099/web-performance-eval/",
    activeRule: "/web-performance-eval",
  },
  // {
  //   name: "app-vue-history",
  //   entry: "http://localhost:2222",
  //   activeRule: "/app-vue-history",
  // },
];

const apps = microApps.map((item) => {
  return {
    ...item,
    container: "#appContainer", // 子应用挂载的div
    props: {
      routerBase: item.activeRule, // 下发基础路由
      data: { store },
    },
  };
});

export default apps;
