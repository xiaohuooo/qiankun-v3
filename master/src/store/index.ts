import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
export default createStore({
  state: {
    commonData: {
      // parent: 1
    },
  },
  getters: {},
  mutations: {
    setCommonData(state, val) {
      state.commonData = val;
    },
  },
  actions: {},
  modules: {},
  plugins: [
    createPersistedState({
      key: "saveInfo",
    }),
  ],
});
