import _ from 'lodash-es';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

export function getTableListPage(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/tableManage/paged/list`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function saveTable(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/tableManage/add`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function deleteTableApi(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/tableManage/delete`,
    method: 'post',
    data,
  });
}

export function validateNameRepeat(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/tableManage/name/check`,
    method: 'post',
    data,
  });
}

export function getHiveData(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/hiveData/top/list`,
    method: 'post',
    data,
  });
}
