import _ from 'lodash-es';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

export function getTaskListPage(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/paged/list`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function deleteTaskManagementApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/delete`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function offlineTaskManagementApi(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/offline`,
    method: 'post',
    data,
  });
}

export function onlineTaskManagementApi(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/online`,
    method: 'post',
    data,
  });
}

export function batchOfflineTaskManagementApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/delete`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function batchOnlineTaskManagementApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/online`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function runDataSourceApi(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/start`,
    method: 'post',
    data,
  });
}

export function batchRunAllDataSourceApi(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/wholly/start`,
    method: 'post',
    data,
  });
}

export function updateTaskApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/taskManage/update`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
