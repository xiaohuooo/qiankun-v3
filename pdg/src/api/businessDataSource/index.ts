import _ from 'lodash-es';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

export function getDataSourceListPage(data: API.PageRequest) {
  const params = _.cloneDeep(data);
  if (params?.typeValue) {
    params['type'] = params.typeValue;
    delete params.typeValue;
  }
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/datasource/list`,
      method: 'post',
      data: params,
    },
    {
      isJsonBody: true,
    },
  );
}

export function deleteDataSourceApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/delete`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function testConnectDataSource(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/connection/check`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function saveDataSourceApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/add`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function updateDataSourceApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/update`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function getAllDataSourceList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/list/all`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function getSourceTableList(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/table/list`,
    method: 'post',
    data,
  });
}

export function getAllColumns(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/table/column/list`,
    method: 'post',
    data,
  });
}

export function getAllColumnsById(data) {
  return request({
    url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/cols/queryByTableId`,
    method: 'post',
    data,
  });
}
