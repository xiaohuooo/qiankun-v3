import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 分页查询组织绩效
export function getList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/query/List`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

// 新增组织绩效
export function perfSave(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/save`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

// 删除组织绩效
export function perfDelete(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/delete`,
      method: 'post',
      data,
    },
    {
      successMsg: '删除成功',
      errorMsg: '删除失败',
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
