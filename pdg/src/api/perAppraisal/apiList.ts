import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 分页查询组织绩效
export function queryOrgList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/query/List`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询绩效考核
export function queryAppraisalList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/appraisal/perf/query/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询重点工作绩效考核
export function queryPerformanceList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/performance/appraisal/perf/query/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询得分汇总列表
export function summarizingQuery(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}n/org/goal/score/summarizing/query`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 实绩管理-填报与调整查询页面
export function queryAdjust(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/query/adjust/display`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 实绩管理-填报与调整修改
export function saveAuditInfo(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/approval/modify/audit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

// 绩效实绩-提交审核
export function SaveDraft(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/audit/save/draft`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
