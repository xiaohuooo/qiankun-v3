'use strict';
exports.__esModule = true;
exports.buildExpression = exports.isStateValid = void 0;
const dayAliases_1 = require('./dayAliases');

function isStateValid(e) {
  if (e.type == 'weekly' && e.days.length == 0) return false;
  else return true;
}
exports.isStateValid = isStateValid;
const buildExpression = function (syntax, state) {
  if (syntax == 'basic') {
    if (state.type === 'minutes') {
      return '*/'.concat(state.minuteInterval, ' * * * *');
    }
    if (state.type === 'hourly') {
      return ''.concat(state.minutes, ' */').concat(state.hourInterval, ' * * *');
    }
    if (state.type === 'daily') {
      return ''
        .concat(state.minutes, ' ')
        .concat(state.hours, ' */')
        .concat(state.dayInterval, ' * *');
    }
    if (state.type === 'weekly') {
      var days = state.days
        .map(function (d) {
          return (0, dayAliases_1.toDayNumber)(d).toString();
        })
        .sort()
        .join(',');
      return ''.concat(state.minutes, ' ').concat(state.hours, ' * * ').concat(days);
    }
    if (state.type === 'monthly') {
      let day = 0;
      if (state.lastDay) {
        day = 'L';
      } else if (state.days.length > 0) {
        day = state.days.join(',');
      } else {
        day = state.day;
      }

      // return "".concat(state.minutes, " ").concat(state.hours, " ").concat(state.day, " */").concat(state.monthInterval, " *");
      return ''
        .concat(state.minutes, ' ')
        .concat(state.hours, ' ')
        .concat(day, ' */')
        .concat(state.monthInterval, ' *');
    }
    if (state.type === 'advanced') {
      return state.cronExpression;
    }
    throw 'unknown event type: '.concat(state);
  } else if (syntax === 'quartz') {
    if (state.type === 'minutes') {
      return '0 0/'.concat(state.minuteInterval, ' * * * ?');
    }
    if (state.type === 'hourly') {
      return '0 '
        .concat(state.minutes, ' ')
        .concat(state.hours, '/')
        .concat(state.hourInterval, ' * * ?');
    }
    if (state.type === 'daily') {
      return '0 '
        .concat(state.minutes, ' ')
        .concat(state.hours, ' */')
        .concat(state.dayInterval, ' * ?');
    }
    if (state.type === 'weekly') {
      var days = state.days
        .map(function (d) {
          return (0, dayAliases_1.toDayNumber)(d);
        })
        .sort()
        .map(function (d) {
          return (0, dayAliases_1.toDayAlias)(d);
        })
        .join(',');
      return '0 '.concat(state.minutes, ' ').concat(state.hours, ' ? * ').concat(days);
    }
    if (state.type === 'monthly') {
      let day = 0;

      // console.log(state);

      // if (state.lastDay) {
      //     day = 'L'
      // } else {
      //     day = state.days.join(",")
      // }

      if (state.lastDay) {
        day = 'L';
      } else if (state.days.length > 0) {
        day = state.days.join(',');
      } else {
        day = state.day;
      }

      // return "0 ".concat(state.minutes, " ").concat(state.hours, " ").concat(state.day, " */").concat(state.monthInterval, " ?");
      return '0 '
        .concat(state.minutes, ' ')
        .concat(state.hours, ' ')
        .concat(day, ' */')
        .concat(state.monthInterval, ' ?');
    }
    if (state.type === 'advanced') {
      return state.cronExpression;
    }
    throw 'unknown event type: '.concat(state);
  }
  throw 'unknown syntax: '.concat(syntax);
};
exports.buildExpression = buildExpression;
