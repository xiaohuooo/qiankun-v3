export type LocaleType = keyof typeof localeMap;

export const localeMap = {
  zh_CN: 'zh_CN',
  en_US: 'en_US',
} as const;

export const localeList = [
  {
    lang: localeMap.en_US,
    label: 'English',
    icon: '🇺🇸',
    title: 'Language',
  },
  {
    lang: localeMap.zh_CN,
    label: '简体中文',
    icon: '🇨🇳',
    title: '语言',
  },
] as const;

export const configuration = {
  copyright: `${new Date().getFullYear()} <a href="https://www.obei.com.cn" target="_blank">
        <span style="color:#1890ff">欧冶工业品股份有限公司</span></a>&nbsp;版权所有 &nbsp;
        <a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank"><span style="color:#1890ff">沪ICP备2020033177号</span></a>`,
} as const;
