export default {
  'views/businessDataSource/ODSManagement/taskManagement': () =>
    import('@/views/ODSManagement/taskManagement/index.vue'),
  'views/businessDataSource/ODSManagement/tableManagement': () =>
    import('@/views/ODSManagement/tableManagement/index.vue'),
} as const;
