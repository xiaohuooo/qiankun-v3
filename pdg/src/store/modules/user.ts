import { defineStore } from 'pinia';

import type { RouteRecordRaw } from 'vue-router';
import { store } from '@/store';
import { login } from '@/api/login';
import {
  ACCESS_TOKEN_KEY,
  ACCESS_USERNAME_KEY,
  ACCESS_PERMS_KEY,
  ACCESS_USER_INFO_KEY,
  ACCESS_DICT_ALL_KEY,
} from '@/enums/cacheEnum';
import { Storage } from '@/utils/Storage';
import { getUsermenu, logout } from '@/api/account';
import { getUserDetail } from '@/api/system/user';
import { getDictAll } from '@/api/common';

import { generatorDynamicRouter } from '@/router/generator-router';
import { resetRouter } from '@/router';

interface UserState {
  token: string;
  name: string;
  avatar: string;
  // like [ 'sys:user:add', 'sys:user:update' ]
  perms: string[];
  menus: RouteRecordRaw[];
  userInfo: any;
}

export const useUserStore = defineStore({
  id: 'user',
  state: (): UserState => ({
    token: Storage.get(ACCESS_TOKEN_KEY, null),
    name: Storage.get(ACCESS_USERNAME_KEY, ''),
    avatar: '',
    perms: [],
    menus: [],
    userInfo: {},
  }),
  getters: {
    getToken(): string {
      return this.token;
    },
    getAvatar(): string {
      return this.avatar;
    },
    getName(): string {
      return this.name;
    },
    getPerms(): string[] {
      return this.perms;
    },
  },
  actions: {
    /** 清空token及用户信息 */
    resetToken() {
      this.avatar = this.token = this.name = '';
      this.perms = [];
      this.menus = [];
      this.userInfo = {};
      Storage.clear();
    },
    setUserInfo(info) {
      this.userInfo = info;
    },
    setAvatar(avatar) {
      this.userInfo.avatar = avatar;
    },
    /** 登录成功保存token */
    setToken(token: string) {
      this.token = token ?? '';
      const ex = 7 * 24 * 60 * 60 * 1000;
      Storage.set(ACCESS_TOKEN_KEY, this.token, ex);
    },
    /** 登录成功保存用户名 */
    setUserName(name: string) {
      this.name = name ?? '';
      Storage.set(ACCESS_USERNAME_KEY, this.name);
    },
    /** 登录成功保存权限 */
    setPerms(perms: string[]) {
      this.perms = perms ?? [];
      Storage.set(ACCESS_PERMS_KEY, this.perms);
    },
    /** 登录 */
    async login(params: API.LoginParams) {
      try {
        const res = await login(params);
        if (`${res.code}` != 'OK') {
          throw new Error(res.message);
        }
        this.setToken(res.data.token);

        Storage.set(ACCESS_USER_INFO_KEY, res.data.user);

        this.setUserName(params.account);

        const perms = res.data.permissions;
        this.setPerms(perms);

        return this.afterLogin();
      } catch (error) {
        return Promise.reject(error);
      }
    },
    /** 登录成功之后, 获取用户信息以及生成权限路由 */
    async afterLogin() {
      try {
        const name = Storage.get(ACCESS_USERNAME_KEY, '');

        // const wsStore = useWsStore();

        const user = Storage.get(ACCESS_USER_INFO_KEY, null);

        const [userInfo, vuerouters, dictAll] = await Promise.all([
          getUserDetail(name),
          getUsermenu(user.userId),
          getDictAll(),
        ]);

        Storage.set(ACCESS_DICT_ALL_KEY, dictAll.data);
        // console.log(dictAll.data);

        const perms = Storage.get(ACCESS_PERMS_KEY, []);
        this.setPerms(perms);

        this.name = userInfo.username;
        this.avatar = userInfo.avatar;
        this.userInfo = userInfo;

        const menus: API.Menu[] = [];

        const realRouters = vuerouters[0];

        for (let i = 0; i < realRouters.children.length; i++) {
          const tmpMenus = this.toFlatMenu(realRouters.children[i]);
          menus.push(...tmpMenus);
        }

        const generatorResult = await generatorDynamicRouter(menus);
        this.menus = generatorResult.menus.filter((item) => !item.meta?.hideInMenu);

        // !wsStore.client && wsStore.initSocket();

        return { menus, perms, userInfo };
      } catch (error) {
        return Promise.reject(error);
      }
    },
    /** 登出 */
    async logout() {
      const userInfo = Storage.get(ACCESS_USER_INFO_KEY, null);

      if (userInfo) {
        await logout(userInfo.userId);
      }

      // const wsStore = useWsStore();
      // wsStore.closeSocket();
      this.resetToken();
      resetRouter();
    },
    toFlatMenu(router: API.VueRouter): API.Menu[] {
      const menus: API.Menu[] = [];
      const menu: API.Menu = {
        createTime: new Date(new Date().getTime()),
        updateTime: new Date(new Date().getTime()),
        id: router.id,
        parentId: router.parentId,
        name: router.name,
        router: router.path,
        type: router.children?.length > 0 ? 0 : 1,
        icon: router.icon ? router.icon : 'right',
        orderNum: 1000,
        perms: '',
        keepAlive: router.keepAlive, //之前是固定值10，所以怎么配置也没用  api/account/model.d.ts新增两个参数-30。31行
        showInSide: router.showInSide, //之前是固定值1，所以怎么配置也没用
        viewPath: router.component,
      };

      menus.push(menu);

      if (router.children) {
        for (let i = 0; i < router.children.length; i++) {
          const childMenus = this.toFlatMenu(router.children[i]);
          menus.push(...childMenus);
        }
      }

      return menus;
    },
  },
});

// 在组件setup函数外使用
export function useUserStoreWithOut() {
  return useUserStore(store);
}
