// import Vue from 'vue';
import { createVNode } from 'vue';
import { ExclamationCircleOutlined } from '@ant-design/icons-vue';
import { Modal } from 'ant-design-vue';
import { deleteTable, openModal, editDataSource } from './methods';
import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns = (auth, dynamicTableInstance) => {
  return [
    {
      title: '库名',
      dataIndex: 'databaseName',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '表名',
      width: 200,
      align: 'center',
      ellipsis: true,
      dataIndex: 'tableName',
    },
    {
      title: '表中文名',
      dataIndex: 'tableCname',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '描述',
      dataIndex: 'description',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '数据源名称',
      dataIndex: 'sourcedataName',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '源表名称',
      dataIndex: 'sourceTableName',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '创建人',
      dataIndex: 'createName',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '修改人',
      dataIndex: 'updateName',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '修改时间',
      dataIndex: 'updateDate',
      align: 'center',
      width: 200,
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '操作',
      width: 130,
      dataIndex: 'ACTION',
      hideInSearch: true,
      align: 'center',
      fixed: 'right',
      customRender: ({ record }) => {
        return (
          <div>
            <a-button
              v-show={auth.check}
              onClick={() => {
                openModal('tableDetail', '查看表数据', 'table', record.pkId);
              }}
              type="link"
              title="查看表数据"
            >
              <eye-outlined />
            </a-button>
            <a-button
              v-show={auth.update}
              type="link"
              onClick={() => {
                editDataSource(record);
              }}
              title="编辑"
            >
              <form-outlined />
            </a-button>
            <a-button
              v-show={auth.delete}
              type="link"
              title="删除"
              onClick={() => {
                Modal.confirm({
                  title: '删除',
                  icon: createVNode(ExclamationCircleOutlined),
                  content: createVNode(
                    'div',
                    { style: 'color:black;' },
                    '你确认要删除吗？数据删除后将无法进行恢复，请谨慎操作！',
                  ),
                  async onOk() {
                    return deleteTable(record, dynamicTableInstance);
                  },
                });
              }}
            >
              <delete-outlined />
            </a-button>
          </div>
        );
      },
    },
  ];
};
