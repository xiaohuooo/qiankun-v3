export const baseColumns = [
  {
    title: '英文名称',
    dataIndex: 'columnName',
    width: 200,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '字段类型',
    width: 200,
    align: 'center',
    dataIndex: 'columnType',
    hideInSearch: true,
    // customRender: ({ record }) => {
    //   return (
    //     <a-select
    //       v-model:value={record.columnType}
    //       filter-option
    //       allow-clear
    //       placeholder="请选择字段类型"
    //     >
    //       {[{ label: 'int', value: 'int' }].map((field) => {
    //         return (
    //           <a-select-option key={field.label} value={field.label}>
    //             {field.value}
    //           </a-select-option>
    //         );
    //       })}
    //     </a-select>
    //   );
    // },
  },
  {
    title: '字段描述',
    dataIndex: 'columnComment',
    width: 200,
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      return (
        <a-input
          v-model:value={record.columnComment}
          onChange={() => {
            record['description'] = record.columnComment;
          }}
        ></a-input>
      );
    },
  },
  {
    title: '主键',
    dataIndex: 'isPrimaryKeyValue',
    width: 200,
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      return (
        <a-checkbox
          v-model:checked={record.isPrimaryKeyValue}
          onChange={() => {
            record.isPrimaryKey = record.isPrimaryKeyValue ? 1 : 0;
            record['isPk'] = record.isPrimaryKeyValue ? 1 : 0;
          }}
        ></a-checkbox>
      );
    },
  },
];
