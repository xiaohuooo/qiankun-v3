import { reactive } from 'vue';
import _ from 'lodash-es';
import { message as $message } from 'ant-design-vue';
import { getValid } from './valid';
import type { LoadDataParams } from '@/components/core/dynamic-table';
import {
  getTableListPage,
  // testConnectDataSource,
  // saveDataSourceApi,
  // updateDataSourceApi,
  deleteTableApi,
} from '@/api/ODSManagement/tableManagement';
import { ACCESS_TOKEN_KEY } from '@/enums/cacheEnum';
import { Storage } from '@/utils/Storage';

const validateIP = getValid('validateIP');
const validateName = getValid('validateName');
const validateJson = getValid('validateJson');

export const data = reactive({
  tableRegistrationDialog: {
    visible: false,
    title: '',
    type: '',
  },
  tableDetailDialog: {
    visible: false,
    title: '',
    pkId: '',
  },
  dialog: {
    visible: false,
    title: '',
    type: '',
    confirmLoading: false,
  },
  formData: {
    type: '',
    name: '',
    note: '',
    host: '',
    port: '',
    database: '',
    username: '',
    password: '',
    schema: '',
    extraParams: '',
    pkId: '',
  },
  fileList: [],
  headers: {
    Authentication: Storage.get(ACCESS_TOKEN_KEY),
    Authorization: Storage.get(ACCESS_TOKEN_KEY),
  },
  BASE_URL: process.env.VUE_APP_BASE_API,
  options: [
    { label: '1', value: 'mysql' },
    { label: '2', value: 'db2' },
  ],
  rules: {
    type: [{ required: true, message: '请选择数据源', trigger: 'change' }],
    name: [{ required: true, validator: validateName, trigger: 'blur' }],
    host: [{ required: true, validator: validateIP, trigger: 'blur' }],
    port: [{ required: true, message: '请输入端口', trigger: 'blur' }],
    database: [{ required: true, message: '请输入端口数据库', trigger: 'blur' }],
    username: [{ required: true, message: '请输入用户名', trigger: 'blur' }],
    password: [{ required: true, message: '请输入密码', trigger: 'blur' }],
    extraParams: [{ validator: validateJson, trigger: 'blur' }],
  },
  testLoading: false,
  saveLoading: false,
  deleteLoading: false,
});

// 打开弹窗
export const openModal = (prop, title, type, pkId) => {
  data[`${prop}Dialog`] = {
    visible: true,
    title,
    type,
    pkId,
  };
};

// 查询数据
export const loadTableData = async (params: LoadDataParams) => {
  const {
    code,
    success,
    message,
    data: list,
    total,
  } = await getTableListPage({
    ...params,
  });

  if (code === 'OK' && success) {
    return {
      list,
      pagination: { total, page: params.pageNum, size: params.pageSize },
    };
  } else {
    $message.error(message || data || '查询失败');
  }
};

// 新增
export const add = () => {
  data.dialog = {
    visible: true,
    title: '新增数据源',
    type: 'add',
    confirmLoading: false,
  };
};

// 保存
// export const saveDataSource = async (formDataRef, dynamicTableInstance) => {
//   const flag = await formDataRef.validate();
//   if (flag) {
//     data.saveLoading = true;
//     const params = handleData(data.formData);
//     const { code, success, message } = await saveDataSourceApi(params).finally(() => {
//       data.saveLoading = false;
//     });
//     handleResponse(
//       code,
//       success,
//       message,
//       formDataRef,
//       dynamicTableInstance,
//       '保存成功',
//       '保存失败',
//       true,
//     );
//   }
// };

//修改
// export const updateDataSource = async (formDataRef, dynamicTableInstance) => {
//   const flag = await formDataRef.validate();
//   if (flag) {
//     data.saveLoading = true;
//     const params = handleData(data.formData);
//     const { code, success, message } = await updateDataSourceApi(params).finally(() => {
//       data.saveLoading = false;
//     });
//     handleResponse(
//       code,
//       success,
//       message,
//       formDataRef,
//       dynamicTableInstance,
//       '修改成功',
//       '修改失败',
//       true,
//     );
//   }
// };

// 取消
export const handleCancel = (formDataRef) => {
  data.formData = {
    type: '',
    name: '',
    note: '',
    host: '',
    port: '',
    database: '',
    username: '',
    password: '',
    schema: '',
    extraParams: '',
    pkId: '',
  };
  data.dialog = {
    visible: false,
    title: '',
    type: '',
    confirmLoading: false,
  };
  formDataRef.clearValidate();
  formDataRef.resetFields();
};

// 查看
export const checkData = (record) => {
  data.dialog = {
    visible: true,
    title: '查看数据源',
    type: 'check',
    confirmLoading: false,
  };
  data.formData = _.cloneDeep(record);
};

// 删除
export const deleteTable = async (record, dynamicTableInstance) => {
  data.deleteLoading = true;
  const { code, success, message } = await deleteTableApi({ pkId: record.pkId })
    .finally(() => {
      data.deleteLoading = false;
    })
    .catch(() => {
      return Promise.reject();
    });
  handleResponse(code, success, message, null, dynamicTableInstance, '删除成功', '删除失败', true);
};

// 复制
export const copyDataSource = (record) => {
  data.dialog = {
    visible: true,
    title: '复制数据源',
    type: 'copy',
    confirmLoading: false,
  };
  data.formData = _.cloneDeep(record);
  data.formData.pkId = '';
};

// 编辑
export const editDataSource = (record) => {
  data.dialog = {
    visible: true,
    title: '编辑数据源',
    type: 'edit',
    confirmLoading: false,
  };
  data.formData = _.cloneDeep(record);
  if (data.formData.type == '2') {
    data.rules['schema'] = [{ required: true, message: '请输入逻辑分区', trigger: 'blur' }];
  }
};

// 处理参数
// const handleData = (data) => {
//   const params = _.cloneDeep(data);
//   if (params['extraParams']) params['extraParams'] = JSON.parse(params['extraParams']);
//   for (const [key] of Object.entries(params)) {
//     if (!params[key]) delete params[key];
//   }
//   return params;
// };

// 处理返回值
const handleResponse = (
  code,
  success,
  message,
  formDataRef,
  dynamicTableInstance,
  successMsg,
  errorMsg,
  needCloseAndRefresh,
) => {
  if (code === 'OK' && success) {
    $message.success(successMsg);
    if (needCloseAndRefresh) {
      if (formDataRef) handleCancel(formDataRef);
      dynamicTableInstance.reload();
      return Promise.resolve();
    }
  } else {
    $message.error(message || errorMsg);
    return Promise.reject();
  }
};
