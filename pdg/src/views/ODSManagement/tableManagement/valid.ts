export const getValid = (key) => {
  const validates = {
    validateName: async (_rule, value) => {
      if (value === '') {
        return Promise.reject('请输入数据源名称');
      } else if (!/^[a-zA-Z0-9_\u4e00-\u9fa5]+$/.test(value)) {
        return Promise.reject('仅支持中文、英文、数字、“_”，100个字符以内，不允许重复');
      } else {
        return Promise.resolve();
      }
    },
  };
  return validates[key];
};
