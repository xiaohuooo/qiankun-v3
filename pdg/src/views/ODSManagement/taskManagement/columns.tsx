// import Vue from 'vue';
import { createVNode } from 'vue';
import { ExclamationCircleOutlined } from '@ant-design/icons-vue';
import { Modal } from 'ant-design-vue';
import {
  deleteTaskManagement,
  editTaskManagement,
  offlineTaskManagement,
  onlineTaskManagement,
  batchRunAllDataSource,
  runDataSource,
} from './methods';
import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns = (auth, dynamicTableInstance) => {
  return [
    {
      title: 'Id',
      dataIndex: 'pkId',
      width: 55,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '任务编号',
      width: 200,
      align: 'center',
      ellipsis: true,
      dataIndex: 'taskCode',
      hideInSearch: true,
    },
    {
      title: '任务名称',
      dataIndex: 'taskName',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '任务状态',
      dataIndex: 'onlineStatus',
      width: 200,
      align: 'center',
      hideInSearch: true,
      ellipsis: true,
      customRender: ({ record }) => {
        let color = '';
        let text = '';
        if (record.onlineStatus == 1) {
          color = '#87d068';
          text = '已上线';
        } else if (record.onlineStatus == 0) {
          color = '#f50';
          text = '已下线';
        } else if (record.onlineStatus == -1) {
          color = '#ffe599';
          text = '待上线';
        }
        return <a-badge color={color} text={text}></a-badge>;
      },
    },
    {
      title: '运行状态',
      dataIndex: 'runStatus',
      width: 200,
      align: 'center',
      hideInSearch: true,
      ellipsis: true,
      customRender: ({ record }) => {
        let color = '';
        let text = '';
        if (record.runStatus == 7) {
          color = '#87d068';
          text = '成功';
        } else if (record.runStatus == 6) {
          color = '#f50';
          text = '失败';
        } else if (record.runStatus == 0) {
          color = '#ffe599';
          text = '待运行';
        } else if (record.runStatus == 1) {
          color = '#87d068';
          text = '运行中';
        }
        return <a-badge color={color} text={text}></a-badge>;
      },
    },
    {
      title: '数据表',
      dataIndex: 'tableName',
      width: 200,
      ellipsis: true,
      align: 'center',
    },
    {
      title: '同步方式',
      ellipsis: true,
      dataIndex: 'syncType',
      width: 200,
      align: 'center',
      hideInSearch: true,
      customRender: ({ record }) => {
        return <span>{record.syncType == 1 ? '增量' : record.syncType == 1 ? '全量' : ''}</span>;
      },
    },
    {
      title: '创建人',
      ellipsis: true,
      dataIndex: 'createName',
      align: 'center',
      width: 200,
      hideInSearch: true,
    },
    {
      title: '创建时间',
      ellipsis: true,
      dataIndex: 'createDate',
      align: 'center',
      width: 200,
      hideInSearch: true,
    },
    {
      title: '修改人',
      ellipsis: true,
      dataIndex: 'updateName',
      align: 'center',
      width: 200,
      hideInSearch: true,
    },
    {
      title: '修改时间',
      ellipsis: true,
      dataIndex: 'updateDate',
      align: 'center',
      width: 200,
      hideInSearch: true,
    },
    {
      title: '操作',
      width: 200,
      dataIndex: 'ACTION',
      hideInSearch: true,
      align: 'center',
      fixed: 'right',
      customRender: ({ record }) => {
        return (
          <div>
            <a-button
              disabled={!(auth.batchRunAll && record.runStatus != 1)}
              onClick={() => {
                batchRunAllDataSource(record, dynamicTableInstance);
              }}
              title="全量运行"
              type="link"
            >
              <play-circle-outlined />
            </a-button>
            <a-button
              disabled={!(auth.run && record.runStatus != 1)}
              type="link"
              onClick={() => {
                runDataSource(record, dynamicTableInstance);
              }}
              title="运行"
            >
              <play-circle-outlined />
            </a-button>
            <a-button
              v-show={auth.online && (record.onlineStatus == 0 || record.onlineStatus == -1)}
              loading={record.onlineLoading}
              type="link"
              onClick={() => {
                onlineTaskManagement(record, dynamicTableInstance);
              }}
              title="上线"
            >
              <cloud-download-outlined />
            </a-button>
            <a-button
              v-show={auth.offline && record.onlineStatus == 1}
              loading={record.offlineLoading}
              type="link"
              onClick={() => {
                offlineTaskManagement(record, dynamicTableInstance);
              }}
              title="下线"
            >
              <cloud-download-outlined />
            </a-button>
            <a-button
              v-show={auth.update}
              type="link"
              onClick={() => {
                editTaskManagement(record);
              }}
              title="编辑"
            >
              <form-outlined />
            </a-button>
            <a-button
              v-show={auth.delete}
              type="link"
              title="删除"
              onClick={() => {
                Modal.confirm({
                  title: '删除',
                  icon: createVNode(ExclamationCircleOutlined),
                  content: createVNode(
                    'div',
                    { style: 'color:black;' },
                    '你确认要删除吗？数据删除后将无法进行恢复，请谨慎操作！',
                  ),
                  async onOk() {
                    return deleteTaskManagement(record, dynamicTableInstance);
                  },
                });
              }}
            >
              <delete-outlined />
            </a-button>
          </div>
        );
      },
    },
  ];
};
