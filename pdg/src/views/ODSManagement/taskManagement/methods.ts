import { reactive, computed } from 'vue';
import _ from 'lodash-es';
import { message as $message } from 'ant-design-vue';
import type { LoadDataParams } from '@/components/core/dynamic-table';
import {
  getTaskListPage,
  updateTaskApi,
  deleteTaskManagementApi,
  offlineTaskManagementApi,
  onlineTaskManagementApi,
  runDataSourceApi,
  batchRunAllDataSourceApi,
  batchOfflineTaskManagementApi,
  batchOnlineTaskManagementApi,
} from '@/api/ODSManagement/taskManagement';
import { getAllColumnsById } from '@/api/businessDataSource/index';

export const data = reactive({
  selectedRows: [],
  selectedRowKeys: [],
  dialog: {
    visible: false,
    title: '',
    type: '',
    confirmLoading: false,
  },
  columns: [],
  formData: {
    cronExp: '',
    description: '',
    incrField: '',
    onlineStatus: 0,
    pkId: '',
    processCode: '',
    runStatus: '',
    syncType: 0,
    tableId: '',
    taskName: '',
  },
  oldFormData: {},
  rules: {
    syncType: [{ required: true, message: '请选择同步方式', trigger: 'change' }],
    incrField: [{ required: true, message: '请选择增量方式', trigger: 'change' }],
    cronExp: [{ required: true, message: '请选择运行周期', trigger: 'change' }],
  },
  testLoading: false,
  saveLoading: false,
  deleteLoading: false,
  offlineLoading: false,
  onlineLoading: false,
  runLoading: false,
  batchRunLoading: false,
  batchOfflineLoading: false,
  batchOnlineLoading: false,
  needUpdateTask: computed(() => {
    for (const [key] of Object.entries(data.formData)) {
      if (data.formData[key] !== data.oldFormData[key]) {
        return false;
      }
    }
    return true;
  }),
});

// 勾选数据
export const onSelectChange = (selectedRowKeys, list) => {
  data.selectedRowKeys = selectedRowKeys;
  data.selectedRows = list;
};

// 查询数据
export const loadTableData = async (params: LoadDataParams) => {
  const {
    code,
    success,
    message,
    data: list,
    total,
  } = await getTaskListPage({
    ...params,
  });

  if (code === 'OK' && success) {
    return {
      list,
      pagination: { total, page: params.pageNum, size: params.pageSize },
    };
  } else {
    $message.error(message || data || '查询失败');
  }
};

//修改
export const updateTask = async (formDataRef, dynamicTableInstance) => {
  const flag = await formDataRef.validate();
  if (flag) {
    data.saveLoading = true;
    const params = {};
    for (const [key, value] of Object.entries(data.formData)) {
      if (data.formData[key] !== data.oldFormData[key]) {
        params[key] = value;
      }
    }
    params['pkId'] = data.oldFormData['pkId'];
    const { code, success, message } = await updateTaskApi(params).finally(() => {
      data.saveLoading = false;
    });
    handleResponse(
      code,
      success,
      message,
      formDataRef,
      dynamicTableInstance,
      '修改成功',
      '修改失败',
      true,
    );
  }
};

// 取消
export const handleCancel = (formDataRef) => {
  data.formData = {
    cronExp: '',
    description: '',
    incrField: '',
    onlineStatus: 0,
    pkId: '',
    processCode: '',
    runStatus: '',
    syncType: 0,
    tableId: '',
    taskName: '',
  };
  data.dialog = {
    visible: false,
    title: '',
    type: '',
    confirmLoading: false,
  };
  formDataRef.clearValidate();
  formDataRef.resetFields();
};

// 下线
export const offlineTaskManagement = async (record, dynamicTableInstance) => {
  console.log(record);
  record.offlineLoading = true;
  const { code, success, message } = await offlineTaskManagementApi({ pkId: record.pkId }).finally(
    () => {
      record.offlineLoading = false;
    },
  );
  handleResponse(code, success, message, null, dynamicTableInstance, '下线成功', '下线失败', true);
};

// 上线
export const onlineTaskManagement = async (record, dynamicTableInstance) => {
  console.log(record);
  record.onlineLoading = true;
  const { code, success, message } = await onlineTaskManagementApi({ pkId: record.pkId }).finally(
    () => {
      record.onlineLoading = false;
    },
  );
  handleResponse(code, success, message, null, dynamicTableInstance, '上线成功', '上线失败', true);
};

// 批量上线
export const batchOnlineTaskManagement = async (record, dynamicTableInstance) => {
  console.log(record);
  data.batchOnlineLoading = true;
  const { code, success, message } = await batchOnlineTaskManagementApi([record.pkId]).finally(
    () => {
      data.batchOnlineLoading = false;
    },
  );
  handleResponse(
    code,
    success,
    message,
    null,
    dynamicTableInstance,
    '批量上线成功',
    '批量上线失败',
    true,
  );
};

// 批量下线
export const batchOfflineTaskManagement = async (record, dynamicTableInstance) => {
  console.log(record);
  data.batchOfflineLoading = true;
  const { code, success, message } = await batchOfflineTaskManagementApi([record.pkId]).finally(
    () => {
      data.batchOfflineLoading = false;
    },
  );
  handleResponse(
    code,
    success,
    message,
    null,
    dynamicTableInstance,
    '批量下线成功',
    '批量下线失败',
    true,
  );
};

// 运行
export const runDataSource = async (record, dynamicTableInstance) => {
  console.log(record);
  record.runLoading = true;
  const { code, success, message } = await runDataSourceApi({ pkId: record.pkId }).finally(() => {
    record.runLoading = false;
  });
  handleResponse(code, success, message, null, dynamicTableInstance, '运行成功', '运行失败', true);
};

// 全量运行
export const batchRunAllDataSource = async (record, dynamicTableInstance) => {
  console.log(record);
  record.batchRunLoading = true;
  const { code, success, message } = await batchRunAllDataSourceApi({ pkId: record.pkId }).finally(
    () => {
      record.batchRunLoading = false;
    },
  );
  handleResponse(
    code,
    success,
    message,
    null,
    dynamicTableInstance,
    '全量运行成功',
    '全量运行失败',
    true,
  );
};

// 删除
export const deleteTaskManagement = async (record, dynamicTableInstance) => {
  data.deleteLoading = true;
  const { code, success, message } = await deleteTaskManagementApi([record.pkId])
    .finally(() => {
      data.deleteLoading = false;
    })
    .catch(() => {
      return Promise.reject();
    });
  handleResponse(code, success, message, null, dynamicTableInstance, '删除成功', '删除失败', true);
};

// 编辑
export const editTaskManagement = async (record) => {
  data.dialog = {
    visible: true,
    title: '编辑数据源',
    type: 'edit',
    confirmLoading: false,
  };
  data.formData = {
    cronExp: record.cronExp,
    description: record.description,
    incrField: record.incrField,
    onlineStatus: record.onlineStatus,
    pkId: record.pkId,
    processCode: record.processCode,
    runStatus: record.runStatus,
    syncType: record.syncType,
    tableId: record.tableId,
    taskName: record.taskName,
  };
  data.oldFormData = _.cloneDeep(data.formData);
  // 加载表字段
  const {
    code,
    success,
    message,
    data: list,
  } = await getAllColumnsById({ tableId: record.tableId });
  if (code === 'OK' && success) {
    data.columns = list.map((x) => {
      return {
        label: x.columnName,
        value: x.columnName,
      };
    });
    console.log(list);
  } else {
    $message.error(message || '查询源表失败');
  }
};

// 处理返回值
const handleResponse = (
  code,
  success,
  message,
  formDataRef,
  dynamicTableInstance,
  successMsg,
  errorMsg,
  needCloseAndRefresh,
) => {
  if (code === 'OK' && success) {
    $message.success(successMsg);
    if (needCloseAndRefresh) {
      if (formDataRef) handleCancel(formDataRef);
      dynamicTableInstance.reload();
      return Promise.resolve();
    }
  } else {
    $message.error(message || errorMsg);
    return Promise.reject();
  }
};
