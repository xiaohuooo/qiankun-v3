// import Vue from 'vue';
import { createVNode } from 'vue';
import { ExclamationCircleOutlined } from '@ant-design/icons-vue';
import { Modal } from 'ant-design-vue';
import { checkData, deleteDataSource, copyDataSource, editDataSource } from './methods';
import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns = (auth, dynamicTableInstance) => {
  return [
    {
      title: 'Id',
      dataIndex: 'pkId',
      width: 55,
      align: 'center',
      hideInSearch: true,
      ellipsis: true,
    },
    {
      title: '数据源名称',
      width: 200,
      align: 'center',
      dataIndex: 'name',
      ellipsis: true,
      customRender: ({ record }) => {
        return createVNode(
          'span',
          {
            onClick: () => {
              checkData(record);
            },
            style: 'color: blue; cursor: pointer;',
          },
          record.name,
        );
      },
    },
    {
      title: '数据源类型',
      dataIndex: 'typeValue',
      width: 200,
      align: 'center',
      ellipsis: true,
      formItemProps: {
        component: 'Select',
        componentProps: {
          options: [
            { label: 'mysql', value: '1' },
            { label: 'db2', value: '2' },
          ],
        },
      },
    },
    {
      title: '数据库',
      dataIndex: 'database',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '逻辑分区',
      dataIndex: 'schema',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '描述',
      dataIndex: 'note',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: 'IP',
      dataIndex: 'host',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '端口',
      dataIndex: 'port',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: 'JDBC附加参数',
      dataIndex: 'extraParams',
      width: 200,
      align: 'center',
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '创建人',
      dataIndex: 'createName',
      align: 'center',
      width: 200,
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      align: 'center',
      width: 200,
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '修改人',
      dataIndex: 'updateName',
      align: 'center',
      width: 200,
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '修改时间',
      dataIndex: 'updateDate',
      align: 'center',
      width: 200,
      ellipsis: true,
      hideInSearch: true,
    },
    {
      title: '操作',
      width: 130,
      dataIndex: 'ACTION',
      hideInSearch: true,
      align: 'center',
      fixed: 'right',
      customRender: ({ record }) => {
        return (
          <div>
            <a-button
              v-show={auth.copy}
              onClick={() => {
                copyDataSource(record);
              }}
              type="link"
              title="复制"
            >
              <copy-outlined />
            </a-button>
            <a-button
              v-show={auth.update}
              type="link"
              onClick={() => {
                editDataSource(record);
              }}
              title="编辑"
            >
              <form-outlined />
            </a-button>
            <a-button
              v-show={auth.delete}
              type="link"
              title="删除"
              onClick={() => {
                Modal.confirm({
                  title: '删除',
                  icon: createVNode(ExclamationCircleOutlined),
                  content: createVNode(
                    'div',
                    { style: 'color:black;' },
                    '你确认要删除吗？数据删除后将无法进行恢复，请谨慎操作！',
                  ),
                  async onOk() {
                    return deleteDataSource(record, dynamicTableInstance);
                  },
                });
              }}
            >
              <delete-outlined />
            </a-button>
          </div>
        );
      },
    },
  ];
};
