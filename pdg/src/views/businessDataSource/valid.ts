export const getValid = (key) => {
  const validates = {
    validateIP: async (_rule, value) => {
      if (value === '') {
        return Promise.reject('请输入IP地址');
      } else if (
        !/^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/.test(value)
      ) {
        return Promise.reject('请输入正确IP地址');
      } else {
        return Promise.resolve();
      }
    },
    validateName: async (_rule, value) => {
      if (value === '') {
        return Promise.reject('请输入数据源名称');
      } else if (!/^[a-zA-Z0-9_\u4e00-\u9fa5]+$/.test(value)) {
        return Promise.reject('仅支持中文、英文、数字、“_”，100个字符以内，不允许重复');
      } else {
        return Promise.resolve();
      }
    },
    validateJson: async (_rule, value) => {
      if (!value) {
        return Promise.resolve();
      } else {
        try {
          const toObj = JSON.parse(value);
          if (toObj && typeof toObj === 'object') {
            return Promise.resolve();
          } else {
            return Promise.reject('必须是JSON格式');
          }
        } catch (error) {
          return Promise.reject('必须是JSON格式');
        }
      }
    },
  };
  return validates[key];
};
