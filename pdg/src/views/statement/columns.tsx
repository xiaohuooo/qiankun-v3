import { isEmpty } from 'lodash-es';
import type { TableColumnsType } from 'ant-design-vue';
import dict from '@/utils/dict.js';
// 集团KPI指标
export const columnsKpi: TableColumnsType = [
  {
    title: '序号',
    width: 100,
    customRender: (column) => {
      return column.index + 1;
    },
    align: 'center',
    fixed: 'left',
  },
  { title: '指标名称', width: 200, dataIndex: 'perfGName', ellipsis: true },
  { title: '指标单位', width: 200, dataIndex: 'perfGOrgName' },
  { title: '基数', width: 200, dataIndex: 'cardinalNumber' },
  { title: '目标值', width: 200, dataIndex: 'yearTarget' },
  { title: '权重(%)', width: 200, dataIndex: 'score' },
  { title: '上年度实绩', width: 200, dataIndex: 'lastYearAchievements' },
  { title: '1季度实绩', dataIndex: 'quarter1Achievements', width: 200 },
  { title: '1季度进度完成率', dataIndex: 'quarter1PercentageComplete', width: 200 },
  { title: '1季度业绩情况简要说明', dataIndex: 'age', width: 200, ellipsis: true },
  { title: '2季度实绩', dataIndex: 'quarter2Achievements', width: 200 },
  { title: '2季度进度完成率', dataIndex: 'quarter2PercentageComplete', width: 200 },
  { title: '2季度业绩情况简要说明', dataIndex: 'quarter2Description', width: 200, ellipsis: true },
  { title: '3季度实绩', dataIndex: 'quarter3Achievements', width: 200 },
  { title: '3季度进度完成率', dataIndex: 'quarter3PercentageComplete', width: 200 },
  { title: '3季度业绩情况简要说明', dataIndex: 'quarter3Description', width: 200, ellipsis: true },
  { title: '4季度实绩', dataIndex: 'quarter4Achievements', width: 200 },
  { title: '4季度完成率', dataIndex: 'quarter4PercentageComplete', width: 200 },
  { title: '年度实绩', dataIndex: 'yearAchievements', width: 200 },
  { title: '年度度完成率', dataIndex: 'yearPercentageComplete', width: 200 },
  { title: '年度业绩情况简要说明', dataIndex: 'yearDescription', width: 200, ellipsis: true },
];
// 集团战略任务
export const columnSstrategy: TableColumnsType = [
  {
    title: '序号',
    width: 100,
    customRender: (column) => {
      return column.index + 1;
    },
    align: 'center',
    fixed: 'left',
  },
  { title: '任务名称', width: 200, dataIndex: 'perfGName', ellipsis: true },
  { title: '任务描述', width: 200, dataIndex: 'taskDescriptions', ellipsis: true },
  { title: '工业品责任部门', width: 200, dataIndex: 'superinDeptName' },
  {
    title: '1季度任务完成情况说明',
    dataIndex: 'quarter1Description',
    width: 200,
    customRender({ record }) {
      return <div innerHTML={record.quarter1Description}></div>;
    },
  },
  {
    title: '2季度任务完成情况说明',
    dataIndex: 'quarter2Description',
    width: 200,
    ellipsis: true,
    customRender({ record }) {
      return <div innerHTML={record.quarter2Description}></div>;
    },
  },
  {
    title: '3季度任务完成情况说明',
    dataIndex: 'quarter3Description',
    width: 200,
    ellipsis: true,
    customRender({ record }) {
      return <div innerHTML={record.quarter3Description}></div>;
    },
  },
  {
    title: '年度任务完成情况说明',
    dataIndex: 'quarter4Description',
    width: 200,
    ellipsis: true,
    customRender({ record }) {
      return <div innerHTML={record.quarter4Description}></div>;
    },
  },
  {
    title: '任务完成情况',
    dataIndex: 'quarter4ScheduleStatus',
    width: 200,
    customRender({ record }) {
      const obj = dict.getValue('EMPHASIS_WORK_SELECT_TYPE', record.quarter4ScheduleStatus);
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
];
// 绩效-得分汇总
export const columnScore: TableColumnsType = [
  {
    title: '序号',
    width: 100,
    customRender: (column) => {
      return column.index + 1;
    },
    align: 'center',
    fixed: 'left',
  },
  { title: '被评价单位', width: 200, dataIndex: 'evaluatedOrgName' },
  { title: '一季度总分', dataIndex: 'sedGoalScore', width: 200 },
  { title: '二季度总分', dataIndex: 'sedGoalScore', width: 200 },
  { title: '三季度总分', dataIndex: 'thirdGoalScore', width: 200 },
  { title: '四季度总分', dataIndex: 'fourthGoalScore', width: 200 },
  { title: '年度总分', dataIndex: 'yearGoalScoreSum', width: 200 },
];
// 绩效-打分汇总
export const columnMark: TableColumnsType = [
  { title: '被评价单位', width: 200, dataIndex: 'evaluatedOrgName' },
  { title: '指标名称', width: 200, dataIndex: 'perfGName', ellipsis: true },
  { title: '指标单位', width: 200, dataIndex: 'perfGOrgName' },
  { title: '一季度目标', dataIndex: 'quarter1Target', width: 200, ellipsis: true },
  { title: '一季度实绩', dataIndex: 'quarter1Achievements', width: 200 },
  { title: '一季度得分（权重得分）', dataIndex: 'quarter1GoalScoreSpecial', width: 200 },
  { title: '二季度目标', dataIndex: 'quarter2Target', width: 200, ellipsis: true },
  { title: '二季度实绩', dataIndex: 'quarter2Achievements', width: 200 },
  { title: '二季度得分（权重得分）', dataIndex: 'quarter2GoalScoreSpecial', width: 200 },
  { title: '三季度目标', dataIndex: 'quarter3Target', width: 200, ellipsis: true },
  { title: '三季度实绩', dataIndex: 'quarter3Achievements', width: 200 },
  { title: '三季度得分（权重得分）', dataIndex: 'quarter3GoalScoreSpecial', width: 200 },
  { title: '四季度目标', dataIndex: 'quarter4Target', width: 200, ellipsis: true },
  { title: '四季度实绩', dataIndex: 'quarter4Achievements', width: 200 },
  { title: '四季度得分（权重得分）', dataIndex: 'quarter4GoalScoreSpecial', width: 200 },
  { title: '年度目标', dataIndex: 'yearTarget', width: 200 },
  { title: '年度实绩', dataIndex: 'yearAchievements', width: 200 },
  { title: '年度得分（权重得分）', dataIndex: 'yearGoalScoreSpecial', width: 200 },
];
// 重点激励项目
export const columnStimulate: TableColumnsType = [
  {
    title: '序号',
    width: 100,
    customRender: (column) => {
      return column.index + 1;
    },
    align: 'center',
    fixed: 'left',
  },
  { title: '重点项目', width: 200, dataIndex: 'perfGName', ellipsis: true },
  { title: '牵头部门', width: 200, dataIndex: 'perfGOrgName' },
  { title: '主要参与部门', width: 200, dataIndex: 'perfDeptInvolved' },
  { title: '年度目标', width: 200, dataIndex: 'yearTarget', ellipsis: true },
  { title: '年度完成情况', width: 200, dataIndex: 'yearDescription', ellipsis: true },
  { title: '年度评价意见', width: 200, dataIndex: 'yearStatus' },
  { title: '一季度目标', width: 200, dataIndex: 'quarter1Target', ellipsis: true },
  {
    title: '一季度目标完成情况',
    width: 200,
    dataIndex: 'quarter1ScheduleStatus',
    customRender({ record }) {
      const obj = record.quarter1ScheduleStatus
        ? dict.getItemText('EMPHASIS_WORK_SELECT_TYPE', record.quarter1ScheduleStatus)
        : null;
      return <div>{obj}</div>;
    },
  },
  { title: '一季度完成情况', width: 200, dataIndex: 'quarter1Description', ellipsis: true },
  {
    title: '一季度未完成原因分析',
    width: 200,
    dataIndex: 'quarter1UnfinishedDesc',
    ellipsis: true,
  },
  { title: '一季度评价意见', width: 200, dataIndex: 'quarter1PerfStatus', ellipsis: true },
  // { title: '一季度激励方式', width: 200, dataIndex: '1' },
  { title: '一季度建议奖励（万元）', width: 200, dataIndex: 'quarter1ProposedBonus' },
  { title: '一季度核定奖励（万元）', width: 200, dataIndex: 'quarter1ApprovedBonus' },
  { title: '二季度目标', width: 200, dataIndex: 'quarter2Target', ellipsis: true },
  {
    title: '二季度目标完成情况',
    width: 200,
    dataIndex: 'quarter2ScheduleStatus',
    customRender({ record }) {
      const obj = record.quarter2ScheduleStatus
        ? dict.getItemText('EMPHASIS_WORK_SELECT_TYPE', record.quarter2ScheduleStatus)
        : null;
      return <div>{obj}</div>;
    },
  },
  { title: '二季度完成情况', width: 200, dataIndex: 'quarter2Description', ellipsis: true },
  {
    title: '二季度未完成原因分析',
    width: 200,
    dataIndex: 'quarter2UnfinishedDesc',
    ellipsis: true,
  },
  { title: '二季度评价意见', width: 200, dataIndex: 'quarter2PerfStatus', ellipsis: true },
  // { title: '二季度激励方式', width: 200, dataIndex: '1' },
  { title: '二季度建议奖励（万元）', width: 200, dataIndex: 'quarter2ProposedBonus' },
  { title: '二季度核定奖励（万元）', width: 200, dataIndex: 'quarter2ApprovedBonus' },
  { title: '三季度目标', width: 200, dataIndex: 'quarter3Target', ellipsis: true },
  {
    title: '三季度目标完成情况',
    width: 200,
    dataIndex: 'quarter3ScheduleStatus',
    customRender({ record }) {
      const obj = record.quarter3ScheduleStatus
        ? dict.getItemText('EMPHASIS_WORK_SELECT_TYPE', record.quarter3ScheduleStatus)
        : null;
      return <div>{obj}</div>;
    },
  },
  { title: '三季度完成情况', width: 200, dataIndex: 'quarter3Description', ellipsis: true },
  {
    title: '三季度未完成原因分析',
    width: 200,
    dataIndex: 'quarter3UnfinishedDesc',
    ellipsis: true,
  },
  { title: '三季度评价意见', width: 200, dataIndex: 'quarter3PerfStatus', ellipsis: true },
  // { title: '三季度激励方式', width: 200, dataIndex: '1' },
  { title: '三季度建议奖励（万元）', width: 200, dataIndex: 'quarter3ProposedBonus' },
  { title: '三季度核定奖励（万元）', width: 200, dataIndex: 'quarter3ApprovedBonus' },
  { title: '四季度目标', width: 200, dataIndex: 'quarter4Target', ellipsis: true },
  {
    title: '四季度目标完成情况',
    width: 200,
    dataIndex: 'quarter4ScheduleStatus',
    customRender({ record }) {
      const obj = record.quarter4ScheduleStatus
        ? dict.getItemText('EMPHASIS_WORK_SELECT_TYPE', record.quarter4ScheduleStatus)
        : null;
      return <div>{obj}</div>;
    },
  },
  { title: '四季度完成情况', width: 200, dataIndex: 'quarter4Description', ellipsis: true },
  {
    title: '四季度未完成原因分析',
    width: 200,
    dataIndex: 'quarter4UnfinishedDesc',
    ellipsis: true,
  },
  { title: '四季度评价意见', width: 200, dataIndex: 'quarter4PerfStatus', ellipsis: true },
  // { title: '四季度激励方式', width: 200, dataIndex: '1' },
  { title: '四季度建议奖励（万元）', width: 200, dataIndex: 'quarter4ProposedBonus' },
  { title: '四季度核定奖励（万元）', width: 200, dataIndex: 'quarter4ApprovedBonus' },
];
