declare namespace API {
  type Menu = {
    createTime: Date;
    updateTime: Date;
    id: number;
    parentId: number;
    name: string;
    router: string;
    perms: string;
    /** 当前菜单类型 0: 目录 1 菜单 | 1: 权限 */
    type: 0 | 1 | 2;
    icon: string;
    orderNum: number;
    viewPath: string;
    keepAlive: string;
    showInSide: number;
  };

  // --------------zhujun 分隔线---------------------
  type VueRouter = {
    id: number;
    parentId: number;
    path: string;
    name: string;
    component: string;
    icon: string;
    redirect: string;
    meta: RouterMeta;
    children: VueRouter[];
    keepAlive: string;
    showInSide: number;
  };

  type RouterMeta = {
    closeable: boolean;
    showInSide: number;
  };
}
