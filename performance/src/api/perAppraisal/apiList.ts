import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 分页查询组织绩效
export function queryOrgList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/query/List`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询绩效考核
export function queryAppraisalList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/appraisal/perf/query/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询重点工作绩效考核
export function queryPerformanceList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/performance/appraisal/perf/query/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询得分汇总列表
export function summarizingQuery(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}n/org/goal/score/summarizing/query`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 实绩管理-填报与调整查询页面
export function queryAdjust(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/source/data/query/adjust/display`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 实绩管理-填报与调整修改
export function saveAuditInfo(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/source/data/fieLd/modify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//查询绩效汇总tab详情
export function queryQuarterly(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/appraisal/perf/and/adjust/quarterly/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
// 绩效实绩-调整得分保存
export function SaveAdjust(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/score/approval/adjust`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 绩效汇总-得分调整查询接口
export function QueryAdjust(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/adjust/org/perf/score/approval/detail`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 绩效汇总-得分调整保存接口
export function SureAdjust(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/perf/score/approval/audit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

/******   二期需求  ******/
// 查询得分列表
export function queryScoreList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/perf/score/adjust/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//提交得分调整列表数据
export function submitScoreList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/score/adjust/submit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//提交得分调整列表数据
export function refreScoreList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/score/refresh`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
/** 重点工作 **/
//暂存接口 ---新接口，目前仅用于重点工作
export function saveWorkDraft(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/focus/work/nature/audit/save/draft`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//保存接口 ---新接口，目前仅用于重点工作
export function saveWorkData(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/focus/work/nature/audit/saveFocusWorkSubmit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查询绩效上报列表
// /obei-performance-eval/p/quarter/report/query/by/{orgCode}
export function queryEscalationList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/query/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//查询绩效上报详情
export function queryEscalationDetail(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/get/quarter/report/detail/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//绩效上报填报
export function reportPerformance(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/modify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//驳回
export function rejectReport(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/audit/reject`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//通过
export function passReport(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/audit/pass`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//查询履历
export function queryAuditHistory(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/get/quarter/report/history/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}

//查询工业品战略任务
// /obei-performance-eval/p/report/get/group/priority/work/{reportCode}
export function queryWorkChart(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/group/priority/work/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
