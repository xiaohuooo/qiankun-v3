declare namespace API {
  /** 创建部门参数 */
  type CreateDeptParams = {
    title: string;
    text: string;
    parentId: number;
    parentName: string;
    orderNum: number;
  };

  type TreeDataItem = {
    key?: string;
    title: string;
    value: string;
    orderNum?: number;
    desc?: string;
    children: Array | null;
  };
}
