/**
 * 解析返回数据
 * @param item
 * @param parentId
 */
export const genDeptTreeItem = (item): API.TreeDataItem => {
  const children: API.TreeDataItem[] = [];

  if (item.children) {
    for (let i = 0; i < item.children.length; i++) {
      children.push(genDeptTreeItem(item.children[i]));
    }
  }

  return {
    key: item.code,
    title: item.title,
    value: item.code,
    orderNum: item.order,
    children: children.length ? children : null,
  };
};
//组织树结构，解析为name+code展示形式
export const genTreeCodeItem = (item): API.TreeDataItem => {
  const children: API.TreeDataItem[] = [];

  if (item.children) {
    for (let i = 0; i < item.children.length; i++) {
      children.push(genTreeCodeItem(item.children[i]));
    }
  }

  return {
    key: item.text,
    title: `${item.title}(${item.code})`,
    value: item.text,
    desc: item.desc,
    children: children.length ? children : null,
  };
};

//组织树结构，解析为name展示形式，value也使用name，配置公式处专用，因为对接的5s那边均使用name查询等
export const genTreeNameItem = (item): API.TreeDataItem => {
  const children: API.TreeDataItem[] = [];

  if (item.children) {
    for (let i = 0; i < item.children.length; i++) {
      children.push(genTreeNameItem(item.children[i]));
    }
  }

  return {
    // key: item.code,
    title: item.title,
    value: item.title,
    children: children.length ? children : null,
  };
};
