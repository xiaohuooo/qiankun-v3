/**字段类型*/
export enum TypeEnum {
  TEXT = '1', //字符
  NUMBER = '2', //数值
  DATE = '3', //日期
  DICT = '4', //集值
  TREE = '5', //树形
}
