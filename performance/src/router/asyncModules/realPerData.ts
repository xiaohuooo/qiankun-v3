//组织绩效设定相关路由
export default {
  'views/realPerData/realPerDataView/RealPerDataView': () =>
    import('@/views/realPerData/RealPerDataView.vue'),
  'views/realPerData/WorkDataView': () => import('@/views/realPerData/WorkDataView.vue'),
} as const;
