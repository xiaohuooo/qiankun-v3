/**
 * test module
 */
export default {
  'views/test/TestMergeTable2': () => import('@/views/test/TestMergeTable2.vue'),
  'views/test/TestCron': () => import('@/views/test/TestCron.vue'),
  'views/test/TestQueryFilter': () => import('@/views/test/TestQueryFilter.vue'),
} as const;
