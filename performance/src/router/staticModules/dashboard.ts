import type { RouteRecordRaw } from 'vue-router';
// import { t } from '@/hooks/useI18n';

const moduleName = 'dashboard';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    name: moduleName,
    // redirect: '/home/welcome',
    component: () =>
      import(/* webpackChunkName: "dashboard-welcome" */ '@/views/dashboard/welcome/Index.vue'),
    meta: {
      title: '系统主页',
      icon: 'icon-shouye',
    },
    children: [
      // {
      //   path: 'welcome',
      //   name: `${moduleName}-welcome`,
      //   meta: {
      //     title: t('routes.dashboard.workbench'),
      //     icon: 'icon-shouye',
      //   },
      //   component: () =>
      //     import(/* webpackChunkName: "dashboard-welcome" */ '@/views/dashboard/welcome/Index.vue'),
      // },
    ],
  },
];

export default routes;
