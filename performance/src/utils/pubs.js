import PubSub from 'pubsub-js';

/**
 * 发布消息
 * @param key
 * @param params
 */
const publish = function (key, params) {
  PubSub.publish(key, params);
};
/**
 * 订阅消息
 * @param key
 * @param fun
 */
const subscribe = function (key, fun) {
  PubSub.subscribe(key, fun);
};

/**
 * 卸载订阅
 * @param key
 */
const unsubscribe = function (key) {
  PubSub.unsubscribe(key);
};

/**
 * 清理所有
 */
const clearAll = function () {
  PubSub.clearAllSubscriptions();
};

export default {
  publish,
  subscribe,
  unsubscribe,
  clearAll,
};
