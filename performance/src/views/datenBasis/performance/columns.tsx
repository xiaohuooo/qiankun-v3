import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.CreatePerforParams;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    dataIndex: 'dataSourceCode',
    width: 60,
    fixed: 'left',
  },
  {
    title: '实绩名称',
    dataIndex: 'dataSourceName',
    ellipsis: true,
    width: 120,
  },
  {
    title: '接口计算逻辑说明',
    dataIndex: 'dataSourceDesc',
    ellipsis: true,
    width: 120,
    hideInSearch: true,
  },
  {
    title: '接口地址',
    dataIndex: 'dataSourceAddr',
    width: 120,
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '接口返回值',
    dataIndex: 'seeValue',
    align: 'center',
    width: 60,
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '字段管理',
    dataIndex: 'manageFields',
    align: 'center',
    width: 60,
    hideInSearch: true,
  },
  {
    title: '创建人',
    dataIndex: 'createName',
    width: 60,
    hideInSearch: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createDate',
    width: 120,
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '修改人',
    dataIndex: 'updateName',
    width: 60,
    hideInSearch: true,
  },
  {
    title: '修改时间',
    dataIndex: 'updateDate',
    width: 120,
    ellipsis: true,
    hideInSearch: true,
  },
];
