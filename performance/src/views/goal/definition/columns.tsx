import dayjs from 'dayjs';
import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.DefResultItem;
export type TableColumnItem = TableColumn<TableListItem>;
export const dateFormat = 'YYYY-MM-DD HH:mm:ss';
export const baseColumns: TableColumnItem[] = [
  {
    title: '目标编号',
    dataIndex: 'perfGCode',
    width: 80,
    ellipsis: true,
    fixed: 'left',
    align: 'center',
  },
  {
    title: '目标名称',
    dataIndex: 'perfGName',
    width: 150,
    fixed: 'left',
    align: 'left',
    ellipsis: true,
    customRender({ record }) {
      return (
        <a-tooltip placement="topLeft" title={record.perfGName}>
          {record.perfGName}
        </a-tooltip>
      );
    },
  },
  {
    title: '指标类别',
    dataIndex: 'perfGCategory',
    align: 'center',
    width: 80,
    customRender({ record }) {
      const obj = dict.getValue('INDICATOR_CATEGORY', record.perfGCategory);
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
    formItemProps: {
      component: 'Select',
      componentProps: () => {
        const options = dict.getKeyValueByDictCode('INDICATOR_CATEGORY');
        return {
          options,
        };
      },
    },
  },
  {
    title: '指标类型',
    dataIndex: 'perfGType',
    align: 'center',
    width: 60,
    customRender: ({ record }) => {
      if (record.perfGType != null) {
        const obj = dict.getValue('INDICATOR_TYPE', record.perfGType);
        if (!isEmpty(obj)) {
          return obj.itemText;
        } else {
          return '-';
        }
      }
    },
    formItemProps: {
      component: 'Select',
      componentProps: () => {
        const options = dict.getKeyValueByDictCode('INDICATOR_TYPE');
        return {
          options,
        };
      },
    },
  },
  {
    title: '指标单位',
    dataIndex: 'perfGOrgCode',
    align: 'center',
    width: 70,
    customRender: ({ record }) => {
      if (record.perfGOrgCode != null) {
        const obj = dict.getValue('INDICATOR_UNIT', record.perfGOrgCode);
        if (!isEmpty(obj)) {
          return obj.itemText;
        } else {
          return '-';
        }
      } else {
        return '-';
      }
    },
    formItemProps: {
      component: 'Select',
      componentProps: () => {
        const options = dict.getKeyValueByDictCode('INDICATOR_UNIT');
        return {
          options,
        };
      },
    },
  },
  {
    title: '归口管理部门',
    dataIndex: 'superinDeptCode',
    width: 100,
    align: 'center',
    hideInTable: true,
    formItemProps: {
      component: 'TreeSelect',
    },
  },
  {
    title: '归口管理部门',
    dataIndex: 'superinDeptName',
    width: 100,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '描述',
    dataIndex: 'perfGDesc',
    width: 150,
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '创建人',
    dataIndex: 'createName',
    width: 80,
    ellipsis: true,
    hideInSearch: true,
    align: 'center',
  },
  {
    title: '创建时间',
    dataIndex: 'createDate',
    width: 120,
    ellipsis: true,
    hideInSearch: true,
    align: 'center',
    customRender: ({ record }) => {
      if (!isEmpty(record.createDate)) {
        return dayjs(record.createDate).format(dateFormat);
      } else {
        return `-`;
      }
    },
  },
  {
    title: '修改人',
    dataIndex: 'updateName',
    width: 80,
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '修改时间',
    dataIndex: 'updateDate',
    width: 120,
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
    customRender: ({ record }) => {
      if (!isEmpty(record.updateDate)) {
        return dayjs(record.updateDate).format(dateFormat);
      } else {
        return `-`;
      }
    },
  },

];
