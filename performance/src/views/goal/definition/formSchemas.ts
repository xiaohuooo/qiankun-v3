import { isEmpty } from 'lodash-es';
import type { FormSchema } from '@/components/core/schema-form/';
import type { SelectProps } from 'ant-design-vue';
import dict from '@/utils/dict.js';
import { check } from '@/api/goal/definition';

export const defSchemas: FormSchema<API.CreateDefinitionParams>[] = [
  {
    field: 'perfGName',
    component: 'Input',
    label: '目标名称',
    // rules: [{ required: true, type: 'string', max: 100 }],
    colProps: {
      span: 24,
    },
    labelWidth: 110,
    dynamicRules: ({ formModel }) => {
      return [
        {
          required: true,
          type: 'string',
          max: 100,
          validator: async (_, value) => {
            if (!isEmpty(value)) {
              const params: any = { perfGName: value };
              if (!isEmpty(formModel.perfGCode)) {
                params.perfGCode = formModel.perfGCode;
              }
              const r: any = await check(params);
              if (!r.success || r.data != 'ok') {
                return Promise.reject('目标名称不合法或已存在');
              }
            } else {
              return Promise.reject('目标名称必填');
            }

            return Promise.resolve();
          },
          trigger: 'blur',
        },
      ];
    },
    componentProps: {
      placeholder: '请输入100字符以内，不允许重复',
    },
  },
  {
    field: 'perfGCategory',
    component: 'Select',
    label: '指标类别',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
    labelWidth: 110,
    componentProps: () => {
      const options: SelectProps['options'] = dict.getKeyValueByDictCode('INDICATOR_CATEGORY');
      return {
        options,
      };
    },
  },
  {
    field: 'perfGType',
    component: 'Select',
    label: '指标类型',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
    labelWidth: 110,
    componentProps: () => {
      const options: SelectProps['options'] = dict.getKeyValueByDictCode('INDICATOR_TYPE');
      return {
        options,
      };
    },
  },
  {
    field: 'perfGOrgCode',
    component: 'Select',
    label: '指标单位',
    colProps: {
      span: 12,
    },
    labelWidth: 110,
    componentProps: () => {
      const options: SelectProps['options'] = dict.getKeyValueByDictCode('INDICATOR_UNIT');
      return {
        options,
      };
    },
  },
  {
    field: 'superinDeptCode',
    component: 'TreeSelect',
    label: '归口管理部门',
    colProps: {
      span: 12,
    },
    labelWidth: 110,
    componentProps: {
      fieldNames: {
        label: 'title',
        value: 'value',
      },
      // getPopupContainer: () => document.body,
    },
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'yearApprovedBonus',
    component: 'InputNumber',
    label: '奖金额度',
    labelWidth: 110,
    colProps: {
      span: 12,
    },
    componentProps: {
      placeholder: '请输入奖金额度',
      maxlength: 100,
      style: {
        width: '100%',
      },
    },
  },
  {
    field: 'superinDeptName',
    component: 'Input',
    vShow: false,
  },
  {
    field: 'perfGCode',
    component: 'Input',
    vShow: false,
  },
  {
    field: 'perfGDesc',
    component: 'InputTextArea',
    label: '描述',
    labelWidth: 110,
    componentProps: {
      showCount: true,
      maxlength: 500,
      style: {
        width: '100%',
      },
    },
  },
];
