// import dict from '@/utils/dict.js';

export const baseColumns = [
  {
    title: '编号',
    dataIndex: 'orgCode',
    width: 80,
    ellipsis: true,
    fixed: 'left',
    align: 'center',
  },
  {
    title: '名称',
    dataIndex: 'orgName',
    ellipsis: true,
    width: 140,
  },
  {
    title: '年份',
    dataIndex: 'period',
    ellipsis: true,
    align: 'center',
    width: 80,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        picker: 'year',
        format: 'YYYY',
      },
    },
  },
  {
    title: '创建人',
    dataIndex: 'createName',
    align: 'center',
    width: 80,
    hideInSearch: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createDate',
    align: 'center',
    width: 120,
    hideInSearch: true,
  },
  {
    title: '修改人',
    dataIndex: 'updateName',
    align: 'center',
    width: 80,
    hideInSearch: true,
  },
  {
    title: '修改时间',
    dataIndex: 'updateDate',
    align: 'center',
    width: 120,
    hideInSearch: true,
  },
];
