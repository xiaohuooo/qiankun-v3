import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.queryOrgList;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    dataIndex: 'orgCode',
    width: 120,
    align: 'center',
    fixed: 'left',
  },
  {
    title: '名称',
    dataIndex: 'orgName',
    ellipsis: true,
    width: 180,
    className: 'text-align',
  },
  {
    title: '年份',
    dataIndex: 'period',
    ellipsis: true,
    align: 'center',
    width: 80,
  },
  {
    title: '创建人',
    dataIndex: 'createName',
    width: 80,
    hideInSearch: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createDate',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '修改人',
    dataIndex: 'updateName',
    width: 100,
    hideInSearch: true,
  },
  {
    title: '修改时间',
    dataIndex: 'updateDate',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
];
