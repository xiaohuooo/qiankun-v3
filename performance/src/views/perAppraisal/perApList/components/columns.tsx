import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.queryScoreDatalist;
export type TableColumnItem = TableColumn<TableListItem>;

export const scoreColumns = [
  {
    title: '目标名称',
    dataIndex: 'perfGName',
    width: 160,
  },
  {
    title: '指标类别',
    dataIndex: 'perfGCategory',
    ellipsis: true,
    width: 120,
    align: 'center',
    customRender({ record }) {
      const obj = dict.getValue('INDICATOR_CATEGORY', record.perfGCategory);
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '权重',
    dataIndex: 'score',
    ellipsis: true,
    align: 'center',
    width: 80,
    customRender({ record }) {
      return record.score ? `${record.score}%` : '-';
    },
  },
  {
    title: '归口管理部门',
    dataIndex: 'superinDeptName',
    width: 120,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '指标单位',
    dataIndex: 'perfGOrgCode',
    align: 'center',
    width: 80,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.perfGOrgCode != null) {
        const obj = dict.getValue('INDICATOR_UNIT', record.perfGOrgCode);
        if (!isEmpty(obj)) {
          return obj.itemText;
        } else {
          return '-';
        }
      } else {
        return '-';
      }
    },
  },
  {
    title: '年度目标',
    dataIndex: 'yearTarget',
    align: 'center',
    width: 140,
    hideInSearch: true,
    customRender({ record }) {
      return record.yearTarget ? record.yearTarget.toLocaleString() : record.yearTarget;
    },
  },
  {
    title: '本季度目标',
    dataIndex: 'quarterTarget',
    align: 'center',
    width: 140,
    hideInSearch: true,
    customRender({ record }) {
      return record.quarterTarget ? record.quarterTarget.toLocaleString() : record.quarterTarget;
    },
  },
  {
    title: '上季度实绩',
    dataIndex: 'upQuarterAchievements',
    align: 'center',
    width: 140,
    hideInSearch: true,
    customRender({ record }) {
      return record.upQuarterAchievements
        ? record.upQuarterAchievements.toLocaleString()
        : record.upQuarterAchievements;
    },
  },
  {
    title: '本季度实绩',
    dataIndex: 'quarterAchievements',
    align: 'center',
    width: 140,
    hideInSearch: true,
    customRender({ record }) {
      return record.quarterAchievements
        ? record.quarterAchievements.toLocaleString()
        : record.quarterAchievements;
    },
  },
];
export const MoreColumns = [
  {
    title: '本季度得分',
    dataIndex: 'quarterScore',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '调整得分',
    dataIndex: 'perfEvalScoreFixValue',
    align: 'center',
    width: 160,
    hideInSearch: true,
  },
  {
    title: '调整理由',
    dataIndex: 'description',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '本季度权重得分',
    dataIndex: 'quarterWeightScore',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
];
