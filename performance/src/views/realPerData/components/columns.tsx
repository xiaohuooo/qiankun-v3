import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.queryReportAdjust;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '实绩指标 ',
    dataIndex: 'fieldNameCn',
    width: 160,
    align: 'center',
  },
  {
    title: '所属单位',
    dataIndex: 'evaluatedOrgName',
    width: 140,
  },
  {
    title: '所属指标',
    dataIndex: 'perfGName',
    width: 100,
    align: 'center',
  },
  {
    title: '指标类型',
    dataIndex: 'fieldType',
    width: 100,
    align: 'center',
    customRender({ record }) {
      const obj = record?.fieldType ? dict.getValue('COLUMN_BUS_TYPE', record?.fieldType) : null;
      if (!isEmpty(obj)) {
        return record.fieldType !== '10' ? (
          <div>{obj.itemText || ''}</div>
        ) : (
          <div style="color:#1890ff">{obj.itemText || ''}</div>
        );
      } else {
        return '-';
      }
    },
  },
  {
    title: '实绩',
    dataIndex: 'fieldValue',
    width: 170,
    align: 'center',
    customRender({ record }) {
      if (record.fieldType !== '10') {
        return record.fieldValue;
      }
    },
  },
];
