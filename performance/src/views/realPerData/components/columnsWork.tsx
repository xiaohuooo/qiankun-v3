import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.queryReportWork;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '重点工作项目 ',
    dataIndex: 'perfGName',
    width: 200,
    // customCell: (record, _index) => {
    //   //合并单元格
    //   return { rowSpan: record.rowSpan };
    // },
  },
  {
    title: '年度目标',
    dataIndex: 'yearEmphasisTarget',
    width: 280,
  },
  {
    title: '季度目标 ',
    dataIndex: 'qEmphasisGoal',
    width: 280,
  },
  {
    title: '目标完成情况自评',
    dataIndex: 'perfEvalScoreFixOpt',
    width: 140,
    align: 'center',
    customRender({ record }) {
      return record?.perfEvalScoreFixOpt
        ? dict.getValue('EMPHASIS_WORK_SELECT_TYPE', record?.perfEvalScoreFixOpt).itemText
        : '-';
    },
  },
  {
    title: '说明材料',
    dataIndex: 'attestationFiles',
    width: 200,
    className: 'text-align',
  },
  {
    title: '完成情况说明',
    dataIndex: 'description',
    width: 200,
    className: 'text-align',
    customRender({ record }) {
      if (!record.description || record.description == '<p><br></p>') {
        return '-';
      } else {
        return <div innerHTML={record.description}></div>;
      }
    },
  },
  {
    title: '未完成原因分析',
    dataIndex: 'unfinishedDescription',
    width: 200,
    className: 'text-align',
    customRender({ record }) {
      if (!record.unfinishedDescription || record.unfinishedDescription == '<p><br></p>') {
        return '-';
      } else {
        return <div innerHTML={record.unfinishedDescription}></div>;
      }
    },
  },
];
