import { request, downloadJson } from '@/utils/request';

export function getSupervisionModel(data: API.PageRequest) {
  return request(
    {
      url: `p/query/file/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
export function getDatasetList(data: API.PageRequest) {
  return request<API.TableListResult<API.DSListPageResult>>(
    {
      url: `p/query/file/list`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
export function AllDownloads(params, filename) {
  const url = `p/batch/file/export/download`;
  downloadJson(url, params, filename);
}
export function Downloads(params, filename) {
  const url = `p/file/export/download`;
  downloadJson(url, params, filename);
}
// 监督成果列表
export function monitoringResDownload(data) {
  return request(
    {
      url: `p/query/monitoringResDownload`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
