import { omit } from 'lodash-es';
import { request } from '@/utils/request';

export function getDSList(data: API.PageRequest) {
  return request<API.TableListResult<API.DSListPageResult>>(
    {
      url: `p/query/source`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function createDS(data: API.CreateDSParams) {
  const rdata = omit(data, ['url,userName,password']) as any;
  rdata.config = {
    apiUrl: data.url,
    userName: data.userName,
    passWord: data.passWord,
  };

  return request(
    {
      url: 'p/add/source',
      method: 'post',
      data: rdata,
    },
    {
      successMsg: '创建成功',
      errorMsg: '服务端异常',
      isJsonBody: true,
    },
  );
}

export function updateDS(data) {
  const rdata = omit(data, ['url,userName,password']) as any;
  rdata.config = {
    apiUrl: data.url,
    userName: data.userName,
    passWord: data.passWord,
  };

  return request(
    {
      url: '/p/update/source',
      method: 'post',
      data: rdata,
    },
    {
      successMsg: '编辑成功',
      errorMsg: '服务端异常',
      isJsonBody: true,
    },
  );
}

export function deleteDS(id) {
  return request(
    {
      url: `p/delete/source?id=${id}`,
      method: 'post',
    },
    {
      successMsg: '删除成功',
      isJsonBody: true,
    },
  );
}

export function checkDSName(name) {
  return request(
    {
      url: `p/check/source?sourceName=${name}`,
      method: 'post',
    },
    {
      isJsonBody: true,
    },
  );
}
