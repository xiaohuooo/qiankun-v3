declare namespace API {
  type DSListPageResultItem = {
    sourceName: string;
    sourceType: string;
    url: string;
    createTime: string;
  };

  type DSListPageResult = DSListPageResultItem[];

  type CreateDSParams = {
    sourceName: string;
    sourceType: string;
    url: string;
    userName: string;
    passWord: string;
  };
}
