import { request, downloadJson } from '@/utils/request';
// 下载中心-数据表三级分类接口
export function queryDataCategory(data) {
  return request(
    {
      url: `p/query/dataset/category/tree`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 下载中心-数据筛选&查询接口
export function getDatasetPreview(data) {
  return request(
    {
      url: `p/dataset/preview`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 下载中心-下载查询结果接口
export function dowloadQueryResult(data) {
  return request(
    {
      url: `p/file/upload/hdfs/by/db2`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 文件下载中心-查询接口
export function queryDownloadList(data) {
  return request(
    {
      url: `p/query/file/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 文件下载中心-下载接口
export function Downloads(params, filename) {
  const url = `p/file/export/download`;
  downloadJson(url, params, filename);
}
