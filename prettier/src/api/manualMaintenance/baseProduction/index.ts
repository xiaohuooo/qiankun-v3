import _ from 'lodash-es';
import dayjs from 'dayjs';
import { request } from '@/utils/request';

export function getBaseProductionListPage(data: API.PageRequest) {
  const params = _.cloneDeep(data);
  if (params['ym']) params['ym'] = dayjs(params['ym']).format('YYYYMM');
  if (params?.baseValue) {
    params['baseList'] = params.baseValue;
    delete params.baseValue;
  }
  return request<API.TableListResult<API.BaseProductionListResult>>(
    {
      url: `n/query/yield`,
      method: 'post',
      data: params,
    },
    {
      isJsonBody: true,
    },
  );
}

export function updateBaseProduction(data) {
  return request(
    {
      url: `p/update/yield`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
