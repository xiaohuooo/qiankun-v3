import _ from 'lodash-es';
import { request } from '@/utils/request';

export function getMaterialMaintenanceListPage(data: API.PageRequest) {
  const params = _.cloneDeep(data);
  if (params?.baseValue) {
    params['baseList'] = params.baseValue;
    delete params.baseValue;
  }
  return request<API.TableListResult<API.MaterialMaintenanceListResult>>(
    {
      url: 'n/query/material',
      method: 'post',
      data: params,
    },
    {
      isJsonBody: true,
    },
  );
}

export function updateMaterialMaintenance(data) {
  return request(
    {
      url: `p/update/material`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
