import { request } from '@/utils/request';
// 查询所有的一级部门
export function queryLevelDept() {
  return request(
    {
      url: `/p/query/zdform/queryLevelDept`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 查询重点工作列表
export function queryZdformQueryDto(data) {
  return request(
    {
      url: `/p/query/zdformQueryDto`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 新增重点工作
export function addZdform(data) {
  return request(
    {
      url: `/p/add/addZdform`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 修改重点工作
export function updateZdform(data) {
  return request(
    {
      url: `/p/update/updateZdform`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 工作填报月份查询
export function queryZdformFillByFillMonth(data) {
  return request(
    {
      url: `/p/query/queryZdformFillByFillMonth`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 填报重点工作填报
export function addZdformFill(data) {
  return request(
    {
      url: `/p/add/addZdformFill`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 修改任务状态
export function updateTaskStatus(data) {
  return request(
    {
      url: `/p/update/updateTaskStatus`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 根据项目Code批量删除项目
export function removeProjects(data) {
  return request(
    {
      url: `/p/remove/removeProjects`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 总体情况
export function overallSituation(data) {
  return request(
    {
      url: `/p/query/overallSituation`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
// 首页看板项目列表
export function queryProjectStatus(data) {
  return request(
    {
      url: `/p/query/queryProjectStatus`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 重点工作看板-下阶段工作提示(新增或修改)
export function addOrRemoveJobTips(data) {
  return request(
    {
      url: `/p/operate/addOrRemoveJobTips`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 重点工作看板-下阶段工作提示(查询)
export function queryJobTips(data) {
  return request(
    {
      url: `/p/query/queryJobTips`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
// 重点工作修改备注
export function updateTaskDesc(data) {
  return request(
    {
      url: `/p/update/updateTaskDesc`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
