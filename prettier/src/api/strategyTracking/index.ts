import { request } from '@/utils/request';

export function getWidget(data) {
  return request(
    {
      url: `n/widget/previewWidget`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

export function queryAggregate(data) {
  return request(
    {
      url: `n/query/aggregate/data/by/param`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 新增
export function query1(data) {
  return request(
    {
      url: `p/save/data/appraise`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 查询
export function query2(data) {
  return request(
    {
      url: `n/query/data/appraise`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 协议续签新增\修改
export function xqquery03(data) {
  return request(
    {
      url: `p/save/data/labor`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 协议续签删除
export function xqquery02(data) {
  return request(
    {
      url: `n/delete/data/labor`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 协议续签查询
export function xqquery01(data) {
  return request(
    {
      url: `n/query/data/labor`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

//工作提示(保存/修改)
export function gztsquery01(data) {
  return request(
    {
      url: `p/save/data/hint`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//工作提示(查询)
export function gztsquery02(data) {
  return request(
    {
      url: `n/query/data/hint`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//pdf保存
export function pdfSave(data) {
  return request(
    {
      url: `p/save/s3file/batch`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//pdf查询
export function pdfQuery(data) {
  return request(
    {
      url: `n/query/s3file/by/${data}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

//工作提示(查询)
export function queryRemark(data) {
  return request(
    {
      url: `n/query/remark`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function saveRemark(data) {
  return request(
    {
      url: `p/save/remark`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 保存配置
export function saveAllocate(data) {
  return request(
    {
      url: `p/save/material/allocate`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 查询配置
export function searchAllocate(data) {
  return request(
    {
      url: `n/query/material/allocate`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 查询是否评论
export function remarkCount(data) {
  return request(
    {
      url: `/n/query/remark/count`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 查询表格合并数据
export function inforReport(data) {
  return request(
    {
      url: `/p/tableConfig/dynamics/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
