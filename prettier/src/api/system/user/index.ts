import { omit } from 'lodash-es';
import { request } from '@/utils/request';

// -----------zhujun 分隔线 --------------
export function getUserDetail(username: string) {
  const data = { username };

  return request<API.UserDetail>(
    {
      url: `user/username`,
      method: 'get',
      data,
    },
    {},
  );
}

export function getUserList(data: API.PageRequest) {
  return request<API.TableListResult<API.UserListPageResult>>(
    {
      url: `user`,
      method: 'get',
      data,
    },
    {},
  );
}

export function getUserListPromise(data: API.PageRequest) {
  return request<API.TableListResult<API.UserListPageResult>>(
    {
      url: `user`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

export function createUser(data: API.CreateUserParams) {
  const rdata = omit(data, ['roles']) as any;
  rdata.roleId = data.roles.join(',');

  return request(
    {
      url: 'user',
      method: 'post',
      data: rdata,
    },
    {
      successMsg: '创建用户成功',
    },
  );
}

export function updateUser(data: API.UpdateAdminInfoParams) {
  const rdata = omit(data, ['roles', 'id']) as any;
  rdata.roleId = data.roles.join(',');
  rdata.userId = data.id;
  return request(
    {
      url: 'user',
      method: 'put',
      data: rdata,
    },
    {
      successMsg: '修改用户成功',
    },
  );
}

export function updateUserPassword(data: API.UpdateAdminUserPassword) {
  return request(
    {
      url: '/user/password',
      method: 'put',
      data,
    },
    {
      successMsg: '操作成功',
    },
  );
}
export function resetPasswords(data: any[]) {
  return request(
    {
      url: '/user/password/reset',
      method: 'put',
      data,
    },
    {
      successMsg: '操作成功',
      isJsonBody: true,
    },
  );
}

export function deleteUsers(userIds: number[]) {
  const ids = userIds.join(',');

  return request(
    {
      url: `user/${ids}`,
      method: 'delete',
    },
    {},
  );
}

export function updateAvatar(data) {
  return request(
    {
      url: 'user/avatar',
      method: 'put',
      data,
    },
    {
      isPromise: true,
      successMsg: '更新头像成功',
      errorMsg: '更新头像失败',
    },
  );
}
export function checkPassword(data: any) {
  return request(
    {
      url: `/user/password/check`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}
