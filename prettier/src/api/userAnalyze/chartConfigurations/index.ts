import { request } from '@/utils/request';

// 查询所有数据集
export function dataSetAll() {
  return request(
    {
      url: `p/query/dataSet/all`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}
// 获取字段
export function dataSetItem(id) {
  return request(
    {
      url: `p/query/dataSetItem?id=${id}`,
      method: 'post',
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}
// 获取图表信息
export function dashboardWidget(id) {
  return request(
    {
      url: `p/widget/dashboardWidget?widgetId=${id}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}

// 获取sql
export function getSqlInfo(data) {
  return request(
    {
      url: `p/datamodel/getSqlInfo`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}

// 获取图表数据
export function getAggregateDataPreview(data) {
  return request(
    {
      url: `p/dashboard/getAggregateDataPreview`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      isPromise: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}
// 保存图表数据（更新）
export function updateWidget(data) {
  return request(
    {
      url: `p/widget/updateWidget`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 保存图表数据
export function saveWidget(data) {
  return request(
    {
      url: `p/widget/saveWidget`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}

// 初始化获取图表数据
export function getAggregateData(data) {
  return request(
    {
      url: `n/dashboard/getAggregateData`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      isPromise: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}

export function getSqlParamTest(data) {
  return request(
    {
      url: `n/dashboard/test`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      errorMsg: (res) => {
        return res.message;
      },
    },
  );
}
