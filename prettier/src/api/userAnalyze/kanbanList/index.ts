import { request } from '@/utils/request';
// 查询
export function getBoardList(data: API.PageRequest) {
  return request<API.TableListResult<API.DSListPageResult>>(
    {
      url: `p/board/getBoardList`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 保存编辑
export function saveBoard(data: API.kanbanListParmas) {
  return request(
    {
      url: `p/board/saveBoard`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
      errorMsg: (r) => {
        return r.message;
      },
    },
  );
}

// 获取所有用户
export function allRoleUsers() {
  return request(
    {
      url: `role/list/allRoleUsers`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 拷贝数据
export function copyBoard(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/board/copyBoard`,
      method: 'post',
      data,
    },
    {
      successMsg: '拷贝成功',
      errorMsg: (res) => {
        return res.message;
      },
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}

// 改变上下线状态
export function upDownLineBoard(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/board/upDownLineBoard`,
      method: 'post',
      data,
    },
    {
      successMsg: '状态修改成功',
      errorMsg: (res) => {
        return res.message;
      },
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 删除
export function deleteBoard(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/board/deleteBoard`,
      method: 'get',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
