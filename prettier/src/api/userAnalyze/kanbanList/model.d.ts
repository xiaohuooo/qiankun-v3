declare namespace API {
  type kanbanListColumns = {
    boardId: string;
    status: string;
    // businessObjList: string[];
    // businessScenes: string[];
    businessObjs: string[];
    bizIds: string[];
  };
  type kanbanListParmas = {
    boardId: string;
  };
  type kanbanListSchemas = {
    boardId: string;
    boardName: string;
    description: string;
    businessObjList: string;
    jurisdiction: string;
    businessScenes: string;
    businessScenes: string;
    roleIds: string;
    userIds: string;
    businessObjs: string;
    bizIds: string;
  };
}
