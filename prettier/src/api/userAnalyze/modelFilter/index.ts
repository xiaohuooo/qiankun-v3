import { request } from '@/utils/request';
// 获取下拉数据
export function getDimensionValues(data) {
  return request(
    {
      url: `p/dataset/getDimensionValues`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
