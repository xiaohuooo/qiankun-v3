/* SupervisionGovern module
 */
export default {
  'views/SupervisionGovern/modelresult/ModelResultNew': () =>
    import('@/views/supervisionGovern/modelresult/ModelResultNew.vue'),
  'views/SupervisionGovern/personalTodo/index': () =>
    import('@/views/supervisionGovern/personalTodo/index.vue'),
  'views/SupervisionGovern/modelGroupTodo/index': () =>
    import('@/views/supervisionGovern/modelGroupTodo/index.vue'),
  'views/SupervisionGovern/toDoHistory/historyLists': () =>
    import('@/views/supervisionGovern/toDoHistory/historyLists.vue'),
  'views/SupervisionGovern/toDoHistory/modelGroupHistoryLists': () =>
    import('@/views/supervisionGovern/toDoHistory/modelGroupHistoryLists.vue'),
  'views/SupervisionGovern/sqlAnalysis/sqlAnalysis': () =>
    import('@/views/supervisionGovern/sqlAnalysis/sqlAnalysis.vue'),
  'views/SupervisionGovern/InfoAll': () => import('@/views/supervisionGovern/InfoAll.vue'),
  'views/supervisionGovern/reportDownload/index': () =>
    import('@/views/supervisionGovern/reportDownload/index.vue'),
} as const;
// 'views/supervisionGovern/SupervisionReport/index': () =>
//   import('@/views/supervisionGovern/SupervisionReport/index.vue'),
