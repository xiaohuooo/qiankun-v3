/**
 * test module
 */
export default {
  'views/strategyTracking/businesstracking/overview': () =>
    import('@/views/strategyTracking/businesstracking/Overview.vue'),
  'views/strategyTracking/businesstracking/regionalcenter': () =>
    import('@/views/strategyTracking/businesstracking/Regionalcenter.vue'),
  'views/strategyTracking/businesstracking/OverMonth': () =>
    import('@/views/strategyTracking/businesstracking/OverMonth.vue'),
} as const;
