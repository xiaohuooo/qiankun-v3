import type { RouteRecordRaw } from 'vue-router';
import { LOGIN_NAME, LOGIN_TRANSMITTER } from '@/router/constant';

/**
 * layout布局之外的路由
 */
export const LoginRoute: RouteRecordRaw = {
  path: '/login',
  name: LOGIN_NAME,
  component: () => import(/* webpackChunkName: "login" */ '@/views/login/index.vue'),
  meta: {
    title: '登录',
  },
};
export const TransmitterRoute: RouteRecordRaw = {
  path: '/transmitter',
  name: LOGIN_TRANSMITTER,
  component: () => import(/* webpackChunkName: "login" */ '@/views/login/transmitter.vue'),
  meta: {
    title: '登录',
  },
};

export default [LoginRoute, TransmitterRoute];
