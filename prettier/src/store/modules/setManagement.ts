import { defineStore } from 'pinia';
import { store } from '@/store';

interface SetManagementState {
  modelData: Object;
  id: String;
  modelConfig: any[];
}
const _modelData = sessionStorage.getItem('modelDataInfo');
export const useSetManagement = defineStore({
  id: 'setManagementState',
  state: (): SetManagementState => ({
    id: '',
    modelData: (_modelData && JSON.parse(_modelData)) || {},
    modelConfig: [],
  }),
  getters: {},
  actions: {
    setId(id: String) {
      this.id = id;
    },
    setModelData(modelData: Object) {
      this.modelData = modelData;
    },
  },
});

// Need to be used outside the setupd
export function useSetManagementStateOut() {
  return useSetManagement(store);
}
