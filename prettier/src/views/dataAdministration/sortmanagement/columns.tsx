export const baseColumns = [
  {
    title: '#',
    dataIndex: 'id',
    width: 95,
    align: 'center',
  },
  {
    title: '分类名称',
    width: 160,
    ellipsis: true,
    align: 'center',
    dataIndex: 'categoryName',
  },
  {
    title: '排序',
    width: 100,
    align: 'center',
    dataIndex: 'orderNum',
  },
  {
    title: '创建人',
    width: 120,
    align: 'center',
    dataIndex: 'createUser',
  },
  {
    title: '创建时间',
    width: 120,
    dataIndex: 'createTime',
    align: 'center',
  },
];
