// 一个入参
const parameter = (username, uservalue, usrtID) => {
  const cans = {
    relations: [
      {
        filterField: username,
        filterValues: [uservalue],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 区间入参
const parameter1 = (username, uservalue, usrtID) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 两个入参
const parameter2 = (username, uservalue, usrtID, username1, uservalue1) => {
  const cans = {
    relations: [
      {
        filterField: username,
        filterValues: [uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 两个入参（区间）
const parameter3 = (username, uservalue, usrtID, username1, uservalue1) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};

export default {
  parameter,
  parameter1,
  parameter2,
  parameter3,
};
