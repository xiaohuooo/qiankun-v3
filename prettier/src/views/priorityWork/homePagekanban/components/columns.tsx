const sharedOnCell = (_) => {
  return { rowSpan: _.projectCodeRowSpan };
};
export const baseColumns = [
  {
    title: '项目名称',
    dataIndex: 'projectName',
    width: 200,
    align: 'center',
    customCell: sharedOnCell,
  },
  { title: '编号', dataIndex: 'taskCode', width: 120, align: 'center', hideInSearch: true },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    width: 350,
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      const title = record.taskTitle;
      const text = record.taskContent.replace(/\n/g, '<br>');
      return (
        <div>
          <div class="task-content-title" innerHTML={title}></div>
          <div innerHTML={text}></div>
        </div>
      );
    },
  },
  { title: '进度', dataIndex: 'fillingContent', width: 350, align: 'center', hideInSearch: true },
  {
    title: '责任单位',
    dataIndex: 'responsibilityDeptCode',
    align: 'center',
    width: 100,
    formItemProps: {
      component: 'Select',
      componentProps: { options: [], mode: 'multiple', optionFilterProp: 'label' },
    },
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.responsibilityDeptName} placement="topLeft">
          {record.responsibilityDeptName}
        </a-tooltip>
      );
    },
  },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 80,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
  },
  { title: '备注', dataIndex: 'taskDesc', width: 200, align: 'center', hideInSearch: true },
];
export const baseColumns60 = [
  {
    title: '项目名称',
    dataIndex: 'projectName',
    width: 200,
    align: 'center',
    customCell: sharedOnCell,
  },
  { title: '编号', dataIndex: 'taskCode', width: 120, align: 'center', hideInSearch: true },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    width: 350,
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      const title = record.taskTitle;
      const text = record.taskContent.replace(/\n/g, '<br>');
      return (
        <div>
          <div class="task-content-title" innerHTML={title}></div>
          <div innerHTML={text}></div>
        </div>
      );
    },
  },
  { title: '进度', dataIndex: 'fillingContent', width: 350, align: 'center', hideInSearch: true },
  {
    title: '责任单位',
    dataIndex: 'responsibilityDeptCode',
    align: 'center',
    width: 100,
    formItemProps: {
      component: 'Select',
      componentProps: { options: [], mode: 'multiple', optionFilterProp: 'label' },
    },
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.responsibilityDeptName} placement="topLeft">
          {record.responsibilityDeptName}
        </a-tooltip>
      );
    },
  },
  {
    title: '状态',
    dataIndex: 'taskStatus',
    align: 'center',
    width: 200,
    hideInTable: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [
          { label: '持续跟踪', value: '10' },
          { label: '重点关注', value: '50' },
        ],
      },
    },
  },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 80,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
  },
  { title: '备注', dataIndex: 'taskDesc', align: 'center', width: 200, hideInSearch: true },
];
export const columnModal = [
  { title: '任务编号', dataIndex: 'taskCode', width: 120, align: 'center' },
  { title: '项目名称', dataIndex: 'projectName', width: 200, align: 'center' },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    width: 320,
    customRender: ({ record }) => {
      const title = record.taskTitle;
      const text = record.taskContent.replace(/\n/g, '<br>');
      return (
        <div>
          <div class="task-content-title" innerHTML={title}></div>
          <div innerHTML={text}></div>
        </div>
      );
    },
  },
  { title: '进度', dataIndex: 'fillingContent', align: 'center', width: 300 },
  { title: '责任单位', dataIndex: 'responsibilityDeptName', align: 'center', width: 100 },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 60,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
  },
  { title: '备注', dataIndex: 'taskDesc', align: 'center', width: 200 },
];
