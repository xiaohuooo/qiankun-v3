const formatter = {
  rate1: (text) => {
    if (text == '-') {
      return text;
    } else {
      return `≥${parseFloat(text).toFixed(1)}`; // 保留一位小数
    }
  },
  rate2: (text) => {
    if (text == '-') {
      return text;
    } else {
      return `≤${parseFloat(text).toFixed(1)}`; // 保留一位小数
    }
  },
  rate3: (text) => {
    if (text == '-') {
      return text;
    } else {
      return `${parseFloat(text).toFixed(1)}%`; // 保留一位小数加百分号
    }
  },
  rate4: (text) => {
    if (text == '-') {
      return text;
    } else {
      return parseFloat(text).toFixed(1); // 保留一位小数
    }
  },
  rate9: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(1); // 保留一位小数
      return parseFloat(resdata).toLocaleString();
    }
  },
  rate5: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(0);
      return parseFloat(resdata).toLocaleString(); // 金额不保留小数位
    }
  },
  rate6: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(0);
      return `≤${parseFloat(resdata).toLocaleString()}`; // 金额不保留小数位
    }
  },
  rate7: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(0);
      return `≥${parseFloat(resdata).toLocaleString()}`; // 金额不保留小数位
    }
  },
  rate8: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(1);
      return parseFloat(resdata).toLocaleString(); // 保留一位有效小数
    }
  },
  rate10: (text) => {
    if (text == '-') {
      return text;
    } else {
      return `${parseFloat(text).toFixed(2)}%`; // 保留一位小数加百分号
    }
  },
};

const specialFormatter1 = {
  name: 'money',
  special: {
    0: 'rate1',
    1: 'rate1',
    2: 'rate1',
    3: 'rate6',
    4: 'rate2',
    5: 'rate6',
    6: 'rate7',
    7: 'rate1',
    8: 'rate6',
    9: 'rate1',
    10: 'rate1',
    11: 'rate7',
    12: 'rate7',
    13: 'rate1',
  },
};
const specialFormatter2 = {
  name: 'money',
  special: {
    0: 'rate4',
    1: 'rate4',
    2: 'rate4',
    3: 'rate5',
    4: 'rate4',
    5: 'rate5',
    6: 'rate5',
    7: 'rate4',
    8: 'rate5',
    9: 'rate4',
    10: 'rate4',
    11: 'rate5',
    12: 'rate5',
    13: 'rate4',
  },
};

const cellColor = {
  colorGKone: {
    complex: 'true',
    color: {
      3: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      4: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorGk02: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl07month: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'CURMONTH_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl07year: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_PROGRESS_TARGET', 'CURYEAR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl02month: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURMONTH_FIRST_CHK_QUALIFIED_RATE'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl02year: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURYEAR_FIRST_CHK_QUALIFIED_RATE'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl03month: {
    complex: 'true',
    color: {
      all: {
        columns: ['PROGRESS_TARGET', 'NUM2'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl03year: {
    complex: 'true',
    color: {
      all: {
        columns: ['PROGRESS_TARGET', 'NUM13'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorcb01year: [
    ['LT', 90, 'red'],
    ['GT', 103, 'red'],
  ],
  colorcb01: {
    complex: 'true',
    color: {
      all: {
        columns: ['QUARTER_RATE_SORT', 'QUARTER_RATE'],
        expression: '{1}',
        rule: [
          ['GT', '{0}', 'red'],
          ['LT', `{0}*${0.9}`, 'red'],
        ],
      },
    },
  },
  colorcb02month: {
    complex: 'true',
    color: {
      all: {
        columns: ['TON_STEEL_COST_TARGET_DECOMPOSE', 'MONTH_STEEL_COST'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
    },
  },
  colorcb02year: {
    complex: 'true',
    color: {
      all: {
        columns: ['TON_STEEL_COST_TARGET_DECOMPOSE', 'YEAR_TOTAL_STEEL_COST'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
    },
  },
  colorkc01bylas: [['GT', 3.0, 'red']],
  colorljtwo: {
    complex: 'true',
    color: {
      4: {
        columns: ['THREE_MONTH_RECEIVABLE_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
    },
  },
  colorljthree: {
    complex: 'true',
    color: {
      1: {
        columns: ['FOUR_MONTH_RECEIVABLE_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      2: {
        columns: ['FOUR_MONTH_RECEIVABLE_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      4: {
        columns: ['FOUR_MONTH_RECEIVABLE_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      9: {
        columns: ['FOUR_MONTH_RECEIVABLE_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      13: {
        columns: ['FOUR_MONTH_RECEIVABLE_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
    },
  },
  colorljfour: {
    complex: 'true',
    color: {
      4: {
        columns: ['FIVE_PIECE_MONTH_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      7: {
        columns: ['FIVE_PIECE_MONTH_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      9: {
        columns: ['FIVE_PIECE_MONTH_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      13: {
        columns: ['FIVE_PIECE_MONTH_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
    },
  },
  colorljfive: {
    complex: 'true',
    color: {
      4: {
        columns: ['FIVE_PIECE_MONTH_OVER_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      9: {
        columns: ['FIVE_PIECE_MONTH_OVER_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      13: {
        columns: ['FIVE_PIECE_MONTH_OVER_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      14: {
        columns: ['FIVE_PIECE_MONTH_OVER_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
      15: {
        columns: ['FIVE_PIECE_MONTH_OVER_RECEIVABLE_TOTAL_AMT'],
        expression: '{0}',
        rule: [['GT', 0.0, 'red']],
      },
    },
  },
  colortwo: [['GT', 0.0, 'red']],
};

export default {
  formatter,
  specialFormatter1,
  specialFormatter2,
  cellColor,
};
