const yearMonth = () => {
  const begindate = new Date(new Date() - 1000 * 60 * 60 * 24);
  const year = begindate.getFullYear(); //获取当前日期的年
  const month = begindate.getMonth() + 1; //获取当前日期的月
  let year2 = year;
  let month2 = parseInt(month) - 1;
  if (month2 == 0) {
    year2 = parseInt(year2) - 1;
    month2 = 12;
  }
  if (month2 < 10) {
    month2 = `0${month2}`;
  }
  const time = `${year2.toString()}-${month2.toString()}`;
  return time;
};

export default {
  yearMonth,
};
