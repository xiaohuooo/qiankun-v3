// 无点击事件,普通数据插入
const toChartsData = (
  inputdata,
  cehartsData,
  parseFunc01,
  parseFunc02,
  parseFunc03,
  parseFunc04,
) => {
  const columnList = inputdata.columnList;
  const returnData = inputdata.data;
  const cb01 = cehartsData;
  for (let i = 0; i < returnData.length; i++) {
    for (let j = 0; j < columnList.length; j++) {
      if (parseFunc01(columnList[j].name)) {
        cb01.xAxis.push(returnData[i][j]);
      }
      let data = returnData[i][j];
      if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
        data = '-';
      } else {
        data = parseFloat(returnData[i][j]).toFixed(2);
      }
      if (parseFunc02(columnList[j].name)) {
        cb01.series[0].data.push(data);
      }
      if (parseFunc03(columnList[j].name)) {
        cb01.series[1].data.push(data);
      }
      if (parseFunc04(columnList[j].name)) {
        cb01.series[2].data.push(data);
      }
    }
  }
  return cb01;
};

// 参与点击事件
const toChartsData01 = (
  inputdata,
  inputid,
  cehartsData,
  parseFunc01,
  parseFunc02,
  parseFunc03,
  parseFunc04,
) => {
  const columnList = inputdata[inputid].columnList;
  const returnData = inputdata[inputid].data;
  const cb01 = cehartsData;
  for (let i = 0; i < returnData.length; i++) {
    for (let j = 0; j < columnList.length; j++) {
      if (parseFunc01(columnList[j].name)) {
        cb01.xAxis.push(returnData[i][j]);
      }
      let data = returnData[i][j];
      if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
        data = '-';
      } else {
        data = parseFloat(returnData[i][j]).toFixed(2);
      }
      if (parseFunc02(columnList[j].name)) {
        cb01.series[0].data.push(data);
      }
      if (parseFunc03(columnList[j].name)) {
        cb01.series[1].data.push(data);
      }
      if (parseFunc04(columnList[j].name)) {
        cb01.series[2].data.push(data);
      }
    }
  }
  return cb01;
};

// 无点击事件,多条数据插入
const toChartsData02 = (inputdata, cehartsData, parseFunc01, parseFunc02) => {
  const columnList = inputdata.columnList;
  const returnData = inputdata.data;
  let xAxisindex = '';
  let Charindex = '';
  let seriesindex = '';
  for (let j = 0; j < columnList.length; j++) {
    if (columnList[j].name === parseFunc01.c01) {
      xAxisindex = columnList[j].index;
    }
    if (columnList[j].name === parseFunc01.c02) {
      Charindex = columnList[j].index;
    }
    if (columnList[j].name === parseFunc01.c03) {
      seriesindex = columnList[j].index;
    }
  }
  const cb01 = cehartsData;
  for (let i = 0; i < returnData.length; i++) {
    if (returnData[i][Charindex] === returnData[0][Charindex]) {
      cb01.xAxis.push(returnData[i][xAxisindex]); // 月份
    }
    let dataser = returnData[i][seriesindex];
    if (
      returnData[i][seriesindex] == '' ||
      returnData[i][seriesindex] == '-' ||
      returnData[i][seriesindex] == null
    ) {
      dataser = '-';
    } else {
      dataser = parseFloat(returnData[i][seriesindex]).toFixed(2);
    }
    if (returnData[i][xAxisindex] === returnData[0][xAxisindex]) {
      cb01.Chart.push(returnData[i][Charindex]); // 图例
      if (parseFunc02 == 1) {
        cb01.series.push({
          name: returnData[i][Charindex],
          data: [dataser],
          type: 'line',
          smooth: false,
          tooltip: {
            valueFormatter(value) {
              return MonText(value);
            },
          },
        });
      } else if (parseFunc02 == 2) {
        cb01.series.push({
          name: returnData[i][Charindex],
          data: [dataser],
          type: 'bar',
          stack: 'total',
          tooltip: {
            valueFormatter(value) {
              return MonText(value);
            },
          },
        });
      } else if (parseFunc02 == 3) {
        cb01.series.push({ name: returnData[i][Charindex], type: 'bar', data: [dataser] });
      }
      // 内容数据
    } else {
      const index = cb01.Chart.indexOf(returnData[i][Charindex]);
      if (index >= 0) {
        cb01.series[index].data.push(dataser);
      }
    }
  }
  return cb01;
};

const toChartsDataxAxis = (xAxis1) => {
  const xAxis = [];
  for (let i = 0; i < xAxis1.length; i++) {
    const jqdata = xAxis1[i].substring(4);
    switch (jqdata) {
      case '01':
        xAxis.push(`${i + 1}月`);
        break;
      case '02':
        xAxis.push(`${i + 1}月`);
        break;
      case '03':
        xAxis.push(`${i + 1}月`);
        break;
      case '04':
        xAxis.push(`${i + 1}月`);
        break;
      case '05':
        xAxis.push(`${i + 1}月`);
        break;
      case '06':
        xAxis.push(`${i + 1}月`);
        break;
      case '07':
        xAxis.push(`${i + 1}月`);
        break;
      case '08':
        xAxis.push(`${i + 1}月`);
        break;
      case '09':
        xAxis.push(`${i + 1}月`);
        break;
      case '10':
        xAxis.push(`${i + 1}月`);
        break;
      case '11':
        xAxis.push(`${i + 1}月`);
        break;
      case '12':
        xAxis.push(`${i + 1}月`);
        break;
      default:
        break;
    }
  }
  return xAxis;
};

const toCharts12DataxAxis = () => {
  const xAxis = [];
  for (let i = 0; i < 12; i++) {
    xAxis.push(`${i + 1}月`);
  }
  return xAxis;
};
const toCharts12DataSeries = (inputdata) => {
  const data = inputdata;
  for (let i = data.length; i < 12; i++) {
    data.push('-');
  }
  return data;
};

// 完成率保留一位小数位
const XiaoShuText = (text) => {
  if (text === '-') {
    return text;
  } else {
    return parseFloat(text).toFixed(1);
  }
};
// 金额保留整数
const MonText = (text) => {
  if (text === '-') {
    return text;
  } else {
    const datatext = parseFloat(text).toFixed(0);
    return parseInt(datatext).toLocaleString();
  }
};

const echartChartsData = (inputdata, inputid, parseFunc, cehartsData) => {
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  const cb01 = cehartsData;
  for (let i = 0; i < returnData.length; i++) {
    for (let j = 0; j < columnList.length; j++) {
      if (columnList[j].name === parseFunc.c01) {
        // X轴或者Y轴的数据
        cb01.xAxis.push(returnData[i][j]);
      }
      let data = returnData[i][j];
      if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
        data = '-';
      } else {
        data = parseFloat(returnData[i][j]).toFixed(2);
      }
      if (columnList[j].name === parseFunc.c02) {
        cb01.series[0].data.push(data);
      }
      if (columnList[j].name === parseFunc.c03) {
        cb01.series[1].data.push(data);
      }
      if (columnList[j].name === parseFunc.c04) {
        cb01.series[2].data.push(data);
      }
    }
  }
  return cb01;
};

const echartsSeries = (text) => {
  const series = [
    {
      type: 'line',
      data: text,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return XiaoShuText(p.value);
            },
          },
        },
      },
    },
  ];
  return series;
};

const echartsSeries1 = (data1, data2) => {
  const series = [
    {
      name: '目标',
      type: 'line',
      smooth: false,
      data: data1,
      itemStyle: { color: '#ffd44a' },
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
    },
    {
      name: '实绩',
      type: 'bar',
      data: data2,
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      itemStyle: { color: '#59c4e6' },
      label: {
        show: true,
        position: 'top',
        formatter(p) {
          return XiaoShuText(p.value);
        },
      },
      barGap: 0,
      barWidth: '30px',
    },
  ];
  return series;
};

export default {
  toChartsData,
  echartChartsData,
  toChartsData01,
  toChartsData02,
  toChartsDataxAxis,
  toCharts12DataxAxis,
  toCharts12DataSeries,
  XiaoShuText,
  MonText,
  echartsSeries,
  echartsSeries1,
};
