export const baseColumns = [
  {
    title: '待办编号',
    dataIndex: 'id',
    width: 40,
    fixed: 'left',
  },
  {
    title: '关键信息',
    dataIndex: 'recordKeyChinese',
    ellipsis: true,
    width: 120,
    fixed: 'left',
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.recordKeyChinese} placement="topLeft">
          {record.recordKeyChinese}
        </a-tooltip>
      );
    },
  },
  {
    title: '模型组名称',
    dataIndex: 'modelName',
    ellipsis: true,
    width: 80,
    fixed: 'left',
  },
  {
    title: '待办类型',
    ellipsis: true,
    dataIndex: 'todoType',
    width: 120,
  },
  {
    title: '业务用途',
    dataIndex: 'businessPurpose',
    width: 100,
    ellipsis: true,
    customRender: ({ record }) => {
      return record.businessPurpose == '1' ? '疑点模型' : '分析模型';
    },
  },
  {
    title: '上级推送人',
    dataIndex: 'beforeUser',
    width: 80,
    ellipsis: true,
  },
  {
    title: '当前环节',
    dataIndex: 'todoLink',
    ellipsis: true,
    width: 80,
  },
  {
    title: '处理时间',
    dataIndex: 'updateTime',
    width: 100,
    ellipsis: true,
  },
];
