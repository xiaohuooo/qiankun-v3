export const baseColumns = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 30,
    fixed: 'left',
    customRender({ index }) {
      return index + 1;
    },
  },
  {
    title: '报表名称',
    dataIndex: 'fileName',
    ellipsis: true,
    width: 120,
  },
  {
    title: '数据来源',
    dataIndex: 'modelName',
    ellipsis: true,
    width: 80,
    customRender() {
      return '首页';
    },
  },
  {
    title: '文件最新更新时间',
    dataIndex: 'createDate',
    ellipsis: true,
    width: 80,
  },
  {
    title: '文件生成状态',
    dataIndex: 'handlerStatus',
    ellipsis: true,
    width: 120,
    customRender: ({ value }) => {
      let color = '#87d068';
      let text = '';
      if (value == 20) {
        color = '#87d068';
        text = '成功';
      } else if (value == 10) {
        color = '#87d068';
        text = '生成中';
      } else if (value == 30) {
        color = '#f50';
        text = '失败';
      }
      return <a-badge color={color} text={text}></a-badge>;
    },
  },
];
