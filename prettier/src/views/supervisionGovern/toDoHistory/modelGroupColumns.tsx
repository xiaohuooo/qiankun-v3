import dict from '@/utils/dict.js';

export const baseColumns = [
  { title: '待办编号', width: 80, key: 'id', scopedSlots: { customRender: 'id' }, fixed: 'left' },

  {
    title: '模型组名称',
    width: 150,
    dataIndex: 'modelName',
    key: 'modelName',
    ellipsis: true,
  },
  {
    title: '处理意见',
    width: 100,
    dataIndex: 'handleRemark',
    key: 'handleRemark',
    ellipsis: true,
    customRender: ({ record }) => {
      return <a-tooltip title={record.handleRemark}>{record.handleRemark}</a-tooltip>;
    },
  },

  {
    title: '模型组管理员',
    dataIndex: 'targetUsernames',
    ellipsis: true,
    width: 100,
  },
  { title: '处理状态', width: 100, dataIndex: 'todoType', key: 'todoType', ellipsis: true },
  {
    title: '业务用途',
    dataIndex: 'businessPurpose',
    width: 100,
    ellipsis: true,
    customRender: ({ value }) => {
      let text = '分析模型';

      if (value == 1) {
        text = '疑点模型';
      }

      return (
        <a-tooltip title={text} placement="topLeft">
          <span>{text}</span>
        </a-tooltip>
      );
    },
  },
  {
    title: '关键信息',
    width: 120,
    dataIndex: 'recordKeyChinese',
    key: 'recordKeyChinese',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.recordKeyChinese} placement="topLeft">
          {record.recordKeyChinese}
        </a-tooltip>
      );
    },
  },
  {
    title: '当前处理人',
    width: 100,
    dataIndex: 'handleUsername',
    key: 'handleUsername',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.handleUsername} placement="topLeft">
          {record.handleUsername}
        </a-tooltip>
      );
    },
  },
  { title: '当前环节', width: 100, dataIndex: 'todoLink', key: 'todoLink', ellipsis: true },
  {
    title: '整改状态',
    width: 100,
    ellipsis: true,
    key: 'rectifyStatus',
    customRender: ({ record }) => {
      return record.rectifyStatus != '-'
        ? dict.getValue('CLUES_RECTIFICATION_STATUS', record.rectifyStatus).itemText
        : record.rectifyStatus;
    },
  },
  { title: '发起日期', width: 140, dataIndex: 'createTime', key: 'createTime', ellipsis: true },
  { title: '办结日期', width: 180, dataIndex: 'closeTime', key: 'closeTime', ellipsis: true },
  { title: '耗时', width: 130, dataIndex: 'timeConsumer', key: 'timeConsumer', ellipsis: true },
  {
    title: '历史环节',
    width: 150,
    dataIndex: 'historyLink',
    key: 'historyLink',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.historyLink} placement="topLeft">
          {record.historyLink}
        </a-tooltip>
      );
    },
  },
];
