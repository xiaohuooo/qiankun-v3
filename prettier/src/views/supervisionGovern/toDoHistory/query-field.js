import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    { columnCname: '待办编号', columnName: 'id', newType: '2', example: null, dictCode: null },
    {
      columnCname: '问题分类',
      columnName: 'bizIds',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('bizIds'),
    },
    { columnCname: '问题简述', columnName: 'proQua', newType: '1', example: null, dictCode: null },
    {
      columnCname: '模型名称',
      columnName: 'modelName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '所属防线',
      columnName: 'lineOfDefenseType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('lineOfDefense'),
    },
    {
      columnCname: '监督领域',
      columnName: 'bizSystem',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('bizSystem'),
    },
    {
      columnCname: '模型归属单位',
      columnName: 'belongUnit',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('DEPT_TYPE'),
    },
    {
      columnCname: '处理意见',
      columnName: 'handleRemark',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '模型管理员',
      columnName: 'sendTargetUsers',
      newType: '4',
      example: null,
      dictCode: null,
      itemDtoList: [],
    },
    {
      columnCname: '处理状态',
      columnName: 'todoType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('TODO_TYPE'),
    },
    {
      columnCname: '业务用途',
      columnName: 'businessPurpose',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('businessPurpose'),
    },
    {
      columnCname: '责任部门/责任单位',
      columnName: 'resDepCode',
      newType: 'tree',
      example: null,
      itemDtoList: [],
    },
    { columnCname: '责任人', columnName: 'resPerson', newType: '1', example: null, dictCode: null },
    {
      columnCname: '关键信息',
      columnName: 'recordKeyChinese',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '疑似问题',
      columnName: 'problemType',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '当前处理人',
      columnName: 'handleUsername',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '当前环节',
      columnName: 'todoLink',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('CURRENT_LINK'),
    },
    {
      columnCname: '整改状态',
      columnName: 'rectifyStatus',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('CLUES_RECTIFICATION_STATUS'),
    },
    {
      columnCname: '发起日期',
      columnName: 'createTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '办结日期',
      columnName: 'closeTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '耗时',
      columnName: 'timeConsumer',
      newType: '2',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '历史环节',
      columnName: 'historyLink',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('HISTORY_LINK'),
    },
    {
      columnCname: '审计纪检监督部推送标记',
      columnName: 'auditPushFlag',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('AUDIT_PUSH_FLAG'),
    },
  ],
};
export default queryFields;
