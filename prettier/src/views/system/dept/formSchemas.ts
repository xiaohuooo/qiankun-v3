import type { FormSchema } from '@/components/core/schema-form/';
import dict from '@/utils/dict';
const lineOfDefenseOptions = dict.getItemsByDictCode('lineOfDefense').map((item) => {
  return {
    label: item.itemText,
    value: item.itemValue,
  };
});
export const deptSchemas: FormSchema<API.CreateDeptParams>[] = [
  {
    field: 'title',
    component: 'Input',
    label: '部门名称',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'parentId',
    component: 'TreeSelect',
    label: '父级部门',
    colProps: {
      span: 12,
    },
    componentProps: {
      fieldNames: {
        label: 'title',
        value: 'value',
      },
      getPopupContainer: () => document.body,
    },
    rules: [{ required: true, type: 'number' }],
  },
  {
    field: 'deptLineDefense',
    component: 'Select',
    label: '所属防线',
    componentProps: {
      options: lineOfDefenseOptions,
    },
    colProps: {
      span: 12,
    },
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'orderNum',
    component: 'InputNumber',
    label: '排序号',
    defaultValue: 255,
    colProps: {
      span: 12,
    },
    componentProps: {
      style: {
        width: '100%',
      },
    },
  },
];
