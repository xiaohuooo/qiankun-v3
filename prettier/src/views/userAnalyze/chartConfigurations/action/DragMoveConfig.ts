import { chartConfigurations } from '@/store/modules/chartConfigurations';

const DragMoveConfig = (e) => {
  console.log(222222222, e);
  let flag = false;
  const moveData = e.dragged._underlying_vm_;
  const store = chartConfigurations();
  const to = e.to.__draggable_component__.componentData;
  const from = e.from.__draggable_component__.componentData;
  //横纵、轴、过滤设置componentData属性，左侧字段目录未配置此属性，存在则为拖至横纵轴过滤区
  if (to) {
    //如果存在，横纵轴不能重复出现
    if (from && from.title === '过滤') {
      const exist = to.value.some((x) => x.key === moveData.key);
      if (!exist) {
        //横纵轴不能重复出现
        const type = to.title === '横轴' ? 1 : 0;
        const values = store.insertContent[type].value;
        flag = !values.some((x) => x.key === moveData.key);
      }
    } else {
      flag = !to.value.some((x) => x.key === moveData.key);
    }
  } else {
    //左侧目录
    //从横纵过滤区拖来
    if (from) {
      //如果在任意一轴出现，都不能拖
      let c = 0;
      store.insertContent.forEach((x) => {
        c += x.value.filter((x) => x.key === moveData.key).length;
      });
      if (c < 2) {
        flag = check(e, moveData);
      }
    } else {
      //左侧目录内部拖拽
      flag = check(e, moveData);
    }
  }
  return flag;
};

const check = (e, moveData) => {
  let flag = false;
  const toClass = e.to.className.split(',');
  if (moveData.type == 'field') {
    [
      'catalogue',
      'insertContent0',
      'insertContent1',
      'insertContent2',
      'text',
      'number',
      'time',
    ].forEach((item) => {
      if (toClass.indexOf(item) !== -1) {
        flag = true;
      }
    });
  } else if (moveData.type == 'catalogue') {
    ['insertContent0', 'allCatalogue'].forEach((item) => {
      if (toClass.indexOf(item) !== -1) {
        flag = true;
      }
    });
  } else if (moveData.type == 'addField') {
    ['allAddField', 'insertContent0', 'insertContent1', 'insertContent2'].forEach((item) => {
      if (toClass.indexOf(item) !== -1) {
        flag = true;
      }
    });
  }
  return flag;
};
export default DragMoveConfig;
