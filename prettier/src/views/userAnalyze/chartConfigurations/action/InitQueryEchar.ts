import { message as $message } from 'ant-design-vue';
import ChartDataConfig from './ChartDataConfig';
import { getAggregateData } from '@/api/userAnalyze/chartConfigurations'; // api 接口
import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();
const initQueryEchar = async () => {
  const params = {
    reload: false,
    chartType: store.page.chartType ? store.page.chartType : 'table',
    datasetId: store.page.data.datasetId,
    config: {
      ...store.page.data,
      page: {
        pageSize: store.chartData.page.pageSize,
        pageNum: store.chartData.page.pageNum,
      },
    },
  };
  params.config.option.resultLimit = store.chartData.page.pageSize;
  await getAggregateData(params)
    .then((data) => {
      if (data.success) {
        store.chartData.loading = false;
        ChartDataConfig(data, params);
      } else {
        store.chartData.loading = 'error';
        $message.error('数据预览失败');
      }
    })
    .catch(() => {
      store.chartData.loading = 'error';
    });
};
export default initQueryEchar;
