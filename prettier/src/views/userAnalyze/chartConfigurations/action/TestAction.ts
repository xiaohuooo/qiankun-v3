import { message as $message } from 'ant-design-vue';
import querySql from './QuerySql';
import ChartDataConfig from './ChartDataConfig';
import QueryBefore from './QueryBefore';
import NameMontage from './NameMontage';
import { getAggregateDataPreview } from '@/api/userAnalyze/chartConfigurations'; // api 接口
import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();

let params = {} as any;
const paramsReset = async () => {
  params = {
    datasets: '',
    datasourceId: '',
    previewSql: '',
    chartType: '',
    config: {
      rows: [],
      keys: [],
      values: [],
      filters: [],
      columns: [],
      option: {},
      page: {
        pageSize: store.chartData.page.pageSize,
        pageNum: store.chartData.page.pageNum,
      },
    },
  } as any;
  params.previewSql = await querySql();
  params.config.option = {
    chartAxis: {
      max: store.echartConfig.AxisMaxNum,
      min: store.echartConfig.AxisMinimum,
      minInterval: store.echartConfig.AxisSpace,
    },
    colorTheme: store.echartConfig.colorTheme,
    chartLegend: {
      show: store.echartConfig.isShowLegend,
      orient: store.echartConfig.layout,
      left: store.echartConfig.Horizontal,
      top: store.echartConfig.Longitudinal,
      align: store.echartConfig.FontSize,
    },
    tooltip: {
      show: store.echartConfig.dataShow,
      tooltip: store.echartConfig.dataTypes,
    },
    resultLimit: store.chartData.page.pageSize,
    radarMax: store.echartConfig.Maximum,
    merge: store.echartConfig.isMergeCell,
    rinIn: queryRinIn(),
    filters: store.chartData?.config?.option?.filters || [],
    sqlParams: store.sqlParams,
    editorConfig: store.editorConfig,
    echartConfig: store.echartConfig,
  }; // 图表配置
  params.chartType = store.page.chartType ? store.page.chartType : 'table';
  params.config.chartType = store.page.chartType ? store.page.chartType : 'table';
  params.datasets = store.datasets[0].id;
  params.datasourceId = store.datasets[0].sourceId;
  let catalogueFlag = false; // 监听是否存在目录
  store.insertContent.forEach((item, index) => {
    item.value.forEach((t) => {
      switch (index) {
        case 0:
          if (t.type.split(',').indexOf('catalogue') !== -1) {
            catalogueFlag = true;
            t.children.forEach((cItem) => {
              if (cItem.check) {
                params.config.rows.push({
                  id: cItem.sourceId,
                  columnName: cItem.name,
                  columnCname: NameMontage(cItem),
                  filterType: 'eq',
                  sort: t.sort,
                  distinct: t.distinct || false,
                  values: [],
                  custom: null,
                });
              }
            });
          } else {
            params.config.rows.push({
              id: t.sourceId,
              columnName: t.name,
              columnCname: NameMontage(t),
              filterType: 'eq',
              sort: t.sort,
              distinct: t.distinct || false,
              values: [],
              custom: null,
            });
          }
          break;
        case 1:
          params.config.values.push({
            column: t.name,
            columnCname: NameMontage(t),
            sort: t?.sort || null,
            distinct: t?.distinct || false,
            dataFormat: t?.dataFormat || null,
            aggType: t?.aggregateType || 'SUM',
          });
          break;
        case 2:
          // 执行
          t.filters.forEach((filterItem) => {
            if (filterItem.filterType == '为空' || filterItem.filterType == '非空') {
              params.config.filters.push({
                columnName: t.name,
                columnCName: t.sourceName,
                values: ['#NULL'],
                sort: t.sort || 'desc',
                id: t.sourceId,
                filterType: filterItem.filterType == '为空' ? '=' : '≠',
              });
            } else if (filterItem.filterType == '包含' || filterItem.filterType == '不包含') {
              params.config.filters.push({
                columnName: t.name,
                columnCName: t.sourceName,
                values: filterItem.values,
                sort: t.sort || 'desc',
                id: t.sourceId,
                filterType: filterItem.filterType == '包含' ? 'like' : 'not like',
              });
            } else {
              params.config.filters.push({
                columnName: t.name,
                columnCName: t.sourceName,
                values: filterItem.values,
                sort: t.sort || 'desc',
                id: t.sourceId,
                filterType: filterItem.filterType,
              });
            }
          });
          break;
      }
    });
  });
  //下钻目录内容配置——store.chartData.config.option.filters下钻过滤条件
  if (store.chartData?.config?.option?.filters?.length && catalogueFlag) {
    params.config.filters.push(...store.chartData.config.option.filters);
  } else if (store.chartData?.config?.option?.filters) {
    store.chartData.config.option.filters = [];
  }
};
//获取目录
const queryRinIn = () => {
  let rinIn = {};
  store.insertContent[0].value.forEach((item) => {
    if (item.newType === 4) {
      rinIn = item;
    }
  });
  return rinIn;
};
const testAction = async () => {
  if (!QueryBefore()) {
    return;
  }
  store.chartData.loading = true;
  await paramsReset();
  await getAggregateDataPreview(params)
    .then((data) => {
      if (data.success) {
        store.chartData.loading = false;
        ChartDataConfig(data, params);
      } else {
        $message.error('数据预览失败');
        store.chartData.loading = 'error';
        store.chartData.page = {
          pageNum: data.pageNum || 1,
          pageSize: data.pageSize || 10,
          total: data.total || 0,
          pageCount: data.pageCount || 1,
        };
      }
    })
    .catch(() => {
      store.chartData.loading = 'error';
      store.chartData.page = {
        pageNum: 1,
        pageSize: 10,
        total: 0,
        pageCount: 1,
      };
    });
};

export default testAction;
