/* eslint-disable */ 
const W = 12;
const AreaFinder = {};
AreaFinder.find = function (layout, newItem) {
  const x = 0;
  const y = 0;

  // calculate cuccrent container's size
  if (layout.length == 0) {
    return {
      x,
      y,
    };
  }

  const { curMaxHeight, curMaxWidth } = this.calArea(layout);
  const area = [];
  for (var i = 0; i < curMaxHeight; i++) {
    const rowArea = [];
    for (var j = 0; j < W; j++) {
      rowArea.push(true);
    }
    area.push(rowArea);
  }

  // fill the occupied area
  for (var i = 0; i < layout.length; i++) {
    const quadruples = layout[i];
    for (var j = quadruples.y; j < quadruples.h + quadruples.y; j++) {
      for (let k = quadruples.x; k < quadruples.x + quadruples.w; k++) {
        area[j][k] = false;
      }
    }
  }

  // select new Quadruple site
  for (var i = 0; i < curMaxHeight; i++) {
    if (this.calMaxConsecutiveWidth(area, i) < newItem.w) continue;
    for (var j = 0; j < W; j++) {
      if (area[i][j] == true && this.calLocation(area, i, j, newItem)) {
        return {
          x: j,
          y: i,
        };
      }
    }
  }

  return {
    x: 0,
    y: curMaxHeight,
  };
};

AreaFinder.calMaxConsecutiveWidth = function (area, i) {
  let res = 0;
  let index = -1;
  for (let j = 0; j < W; j++) {
    if (area[i][j] == false) index = j;
    else res = Math.max(res, j - index);
  }
  return res;
};

AreaFinder.calLocation = function (area, i, j, newItem) {
  const x = newItem.w;
  const y = newItem.h;

  if (x + j > 12) return false;

  let flag = true;
  for (let row = i; row < Math.min(i + y, area.length); row++) {
    for (let col = j; col < Math.min(j + x, W); col++) {
      if (area[row][col]) {
        continue;
      } else {
        flag = false;
        break;
      }
    }

    if (!flag) {
      break;
    }
  }
  return flag;
};

AreaFinder.calArea = function (layout) {
  let maxh = 0;
  let maxw = 0;

  for (let i = 0; i < layout.length; i++) {
    const item = layout[i];
    const temph = item.h + item.y;
    const tempw = item.x + item.w;
    if (maxh < temph) maxh = temph;
    if (maxw < tempw) maxw = tempw;
  }

  return {
    curMaxHeight: maxh,
    curMaxWidth: maxw,
  };
};
export default AreaFinder;
