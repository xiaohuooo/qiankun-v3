import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

//  ------------zhujun 分隔线---------------------
export function getUsermenu(userId: string) {
  return request<API.VueRouter[]>(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}menu/${userId}`,
      method: 'get',
    },
    {},
  );
}

export function logout(userId: number) {
  const data = { id: userId };

  return request({
    url: `${UriPrefix.URL_PORTAL_PREFIX}logout/id`,
    method: 'get',
    data,
  });
}

export function editUser(data) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}user/profile`,
      method: 'put',
      data,
    },
    {
      isPromise: true,
    },
  );
}
