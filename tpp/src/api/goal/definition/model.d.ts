declare namespace API {
  /** 创建目标定义参数 */
  type CreateDefinitionParams = {
    perfGName: string;
    perfGDesc: string;
    perfGCode: string;
    perfGCategory: string;
    perfGType: string;
    superinDept: string;
    superinDeptCode: string;
    superinDeptName: string;
    perfGOrgCode: string;
    perfGOrgName: string;
    createUser: string;
    createName: string;
  };

  /** 编辑目标定义参数 */
  type UpdateDefinitionParams = CreateDefinitionParams & {
    pkId: number;
    updateName: string;
    updateUser: string;
  };

  /**列表结果*/
  type DefResultItem = UpdateDefinitionParams & {
    createDate: string;
    updateDate: string;
  };

  type DefResultItems = DefResultItem[];
}
