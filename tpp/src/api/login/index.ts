import type { BaseResponse } from '@/utils/request';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
/**
 * @description 登录
 * @param {LoginParams} data
 * @returns
 */
export function login(data: API.LoginParams) {
  return request<BaseResponse<API.LoginResult>>(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}login`,
      method: 'post',
      data,
    },
    { isJsonBody: true },
  );
}
/**
 * 滑块验证码
 * @returns {*}
 */
export function getCheck() {
  return request<BaseResponse<API.LoginResult>>(
    {
      url: `${UriPrefix.URL_AUTH_PREFIX}n/get/captcha`,
      method: 'post',
      data: {},
    },
    { isJsonBody: true, isGetDataDirectly: true },
  );
}
export function doCheck(params) {
  return request<BaseResponse<API.LoginResult>>(
    {
      url: `${UriPrefix.URL_AUTH_PREFIX}n/check/captcha`,
      method: 'post',
      data: params,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
