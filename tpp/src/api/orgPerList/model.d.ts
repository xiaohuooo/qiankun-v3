declare namespace API {
  // --------  add by zhujun
  /** 新增配置 */
  type CreatePreParams = {
    orgCode: string;
    orgName: string;
    period: string;
  };

  /**公式入参*/
  type FormulaParams = {
    ruleCode: string;
    apiCode: string;
    eneityResultFunc: string;
    eneityScoreFunc: string;
    eneityResultFuncDesc: string;
    eneityScoreFuncDesc: string;
  };
  type EmitSchemas = {
    name: string;
  };
}
