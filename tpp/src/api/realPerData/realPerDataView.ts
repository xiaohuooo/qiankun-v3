import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 实绩管理 - 实绩管理列表;
export function getSetting(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/query/setting`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查询组织绩效标签-根据组织编号
export function getBy(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/relation/perf/tag/get/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//  绩效实绩审批-数据审核查询
export function getApproval(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/approval/query`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 绩效实绩审批-数据审核
export function setAudit(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/approval/audit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

// 绩效实绩列表明细-数据审核查询
export function approvalCode(approvalCode) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/approval/query/detail/${approvalCode}`,
      method: 'get',
    },
    {},
  );
}
