/**
 * 解析返回数据
 * @param item
 * @param parentId
 */
export const genDeptTreeItem = (item): API.TreeDataItem => {
  const children: API.TreeDataItem[] = [];

  if (item.children) {
    for (let i = 0; i < item.children.length; i++) {
      children.push(genDeptTreeItem(item.children[i]));
    }
  }

  return {
    key: item.code,
    title: item.title,
    value: item.code,
    orderNum: item.order,
    children: children.length ? children : null,
  };
};
