declare namespace API {
  type DictListPageResultItem = {
    datasets: any;
    businessObjList: any;
    dictName: string;
    dictCode: string;
    dictId: string;
    type: string;
    updateUser: string;
    updateTime: string;
    createUser: string;
    createTime: string;
    itemDtoList: DictItem[];
  };

  type DictListPageResult = DictListPageResultItem[];

  type DictItem = {
    dictItemId: string;
    itemText: string;
    itemValue: string;
    sortOrder: number;
  };

  type CreateDictParams = {
    dictName: string;
    dictCode: string;
    type: number;
    itemList: DictItem[];
  };

  type sqlParamsListPageItem = {
    dictName: string;
    dictCode: string;
    createdUser: string;
    itemList: DictItem[];
  };
}
