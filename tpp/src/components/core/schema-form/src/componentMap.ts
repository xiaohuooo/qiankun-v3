/**
 * Component list, register here to setting it in the form
 */
import {
  Input,
  Select,
  Radio,
  Checkbox,
  AutoComplete,
  Cascader,
  DatePicker,
  InputNumber,
  Switch,
  TimePicker,
  TreeSelect,
  Tree,
  Slider,
  Rate,
  Divider,
  Upload,
} from 'ant-design-vue';

import { TableRowEditor, ApiSelect, DatasetRowEditor, StringView, EditTable } from './components';

const componentMap = {
  Input,
  InputGroup: Input.Group,
  InputPassword: Input.Password,
  InputSearch: Input.Search,
  InputTextArea: Input.TextArea,
  InputNumber,
  AutoComplete,
  Select,
  TreeSelect,
  Tree,
  Switch,
  RadioGroup: Radio.Group,
  Checkbox,
  CheckboxGroup: Checkbox.Group,
  Cascader,
  Slider,
  Rate,
  DatePicker,
  MonthPicker: DatePicker.MonthPicker,
  RangePicker: DatePicker.RangePicker,
  WeekPicker: DatePicker.WeekPicker,
  YearPicker: DatePicker.YearPicker,
  TimePicker,
  Upload,
  Divider,
  ApiSelect,
  StringView,
  TableRowEditor,
  EditTable,
  DatasetRowEditor,
};

export type ComponentMapType = keyof typeof componentMap;

export { componentMap };
